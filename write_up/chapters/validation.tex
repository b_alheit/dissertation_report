\chapter{Model Validation}
It is crucial that FEM model is validated by using 
\section{Validation of Soft Tissue User Material Subroutine}
Writing a User Material Subroutine (UMAT) for Abaqus is a fairly extensive process involving man steps. Hence, it is a particularly error prone process. It is therefore important that one verifies that a UMAT is working correctly before it is used to model a large problem. This has been done in this text by determining analytical solutions for simple loading cases and comparing them with the results that Abaqus produces for these cases. The first case chosen is uniaxial loading and the second is finite shear.

\subsection{Uniaxial Loading}
Uniaxial loading is a useful test case because, in the case where the fibres are either either parallel of perpendicular to the loading direction, the problem has a spectral deformation gradient and Cauchy-stress tensor. In other words the problem only has a small number of non-zero values making it manageable to handle. 

The image in Figure \ref{fig:uni_symm} shows a schematic diagram of uniaxial loading.
\begin{figure}[!htb]
\centering
\includegraphics[width=0.6\linewidth]{tensile_test_symm}
\caption{Schematic diagram of uniaxial loading highlighting the symmetries of the problem.}
\label{fig:uni_symm}
\end{figure}
One issue with uniaxial loading is that all of its boundary conditions are Neumann conditions. This poses an issue when implementing it in a FEM model since FEM models must have a Dirichlet boundary condition in each direction to avoid rigid body motion. The issue can be overcome by noting that if the material's fibres are parallel or perpendicular to the loading direction the problem is symmetric about planes $A$ and $B$. If the problem where modelled in 3 dimensions there would be a third plane of symmetry with its normal parallel to the $z$-axis. This allows one equivalently model a quarter of the problem (or an eighth in 3D) as depicted in Figure \ref{fig:ut_symm}.

\begin{figure}[!htb]
\centering
\begin{subfigure}{0.49\textwidth}
\includegraphics[width=\linewidth]{tensile_test_parallel}
\caption{}
\end{subfigure}
\begin{subfigure}{0.49\textwidth}
\includegraphics[width=\linewidth]{tensile_test_perp} 
\caption{}
\end{subfigure}
\caption{Schematic diagrams of a FEM model of a tensile test done on a transversely isotropic material with a) the fibres parallel to the loading direction and b) the fibres perpendicular to the loading direction}
\label{fig:ut_symm}
\end{figure}

Recall from Equation \ref{eq:par_analytical} that under the modified HGO model the first direct component of the Cauchy Stress can be determined by
\begin{equation}
\sigma_{11} =  \sum_{p=1}^N\mu_p\left[\lambda^{\alpha_p} - \lambda^{-\alpha_p/2}\right] + 2k_1(\lambda^2-1)e^{k_2(\lambda^2-1)^2} \lambda^2
\label{eq:an_par}
\end{equation}
when the material fibres are parallel to the loading direction. Recall from Equation \ref{eq:perp_analytical} that when the material fibres are perpendicular to the loading direction the first direct component of the Cauchy Stress can be determined by
\begin{equation}
\sigma_{11} = \sum_{p=1}^N\mu_p\left[\lambda_1^{\alpha_p} - (\lambda_1\lambda_2)^{-\alpha_p}\right]
\label{eq:an_per}
\end{equation}
where $\lambda_2$ is the second principal stretch which can be determined by using Newton's method to minimise the residual function
\begin{equation}
R(\lambda_2) = \sum_{p=1}^N\mu_p\left[\lambda_2^{\alpha_p} - (\lambda_1\lambda_2)^{-\alpha_p}\right] + 2k_1(\lambda_2^2-1)e^{k_2(\lambda_2^2-1)^2}\lambda_2^2.
\label{eq:lam2_res}
\end{equation}

The problem was modelled in Abaqus using a single 20-node quadratic brick with linear pressure and reduced integration (C3D20RH) of dimensions $1\times1\times1$. The left, bottom and back faces of the element were constrained in $x$, $y$ and $z$ directions, respectively. The right face of the element was prescribed a displacement of $u_1$. A \texttt{Python} script was written that utilised the \texttt{Abaqus Python} API to automatically create an run 20 simulations varying the value of $u_1$. A second \texttt{Python} script was written to extract the relevant data from the resulting 20 output databases and save them in conveniently accessible text files. A final \texttt{Python} script was written which loaded in the data from the text files and plotted it against the analytical solutions in Equations \ref{eq:an_par} and \ref{eq:an_per}. The resulting graph can be seen in Figure \ref{fig:comp_aba_an_ten}.
\begin{figure}[!htb]
\centering
\includegraphics[width=0.8\linewidth]{uni_load}
\caption{Comparison of Results from Abaqus simulations against analytical solutions of a tensile test when fibres are parallel to the loading direction ($||$) and perpendicular to the loading direction ($\perp$). The graph also contains the norm of a vector containing all the Cauchy Stresses produced by Abaqus except for $\sigma_{11}$, denoted as $|\bm{\sigma}_{other}|$.}
\label{fig:comp_aba_an_ten}
\end{figure}

The graph shows that the results from \texttt{Abaqus} match the analytical solutions closely. The graph also includes the norm of the vector $\bm{\sigma}_{other}$, where
\begin{equation}
\bm{\sigma}_{other} = \left[ \begin{array}{c c c c c}
\sigma_{22} & \sigma_{33} & \sigma_{12} & \sigma_{13} & \sigma_{23}
\end{array} \right]^T.
\end{equation}
Since $\sigma_{11}$ is the only non-zero stress $|\bm{\sigma}_{other}|$ should be zero for the entire motion. Figure \ref{fig:comp_aba_an_ten} verifies the \texttt{Abaqus} and the UMAT emulates this correctly.

Something worth noting here as it has not been displayed prior is that the fibres do not offer as much resistance in compression as they do in tension. This is evident when one compares the compressive part of the parallel fibre loading to its tensile part. This is a physically reasonable result -- a string does not offer as must resistance in compression as it does in tension. 

Further validation was done by comparing the results \texttt{Abaqus} produced for $\lambda_2$ for the case where the material fibres are perpendicular to the loading direction with the analytical solution determined be minimising Equation \ref{eq:lam2_res}. The results of this can be seen in Figure \ref{fig:lam2_comp}.
\begin{figure}[!htb]
\centering
\includegraphics[width=0.8\linewidth]{stretch_2}
\caption{Comparison of results for $\lambda_2$ determined by \texttt{Abaqus} with the analytical solution.}
\label{fig:lam2_comp}
\end{figure}
The figure shows that the results from \texttt{Abaqus} compare well with the analytical solution.



\subsection{Plane Strain Simple Shear}

To test that the UMAT was capturing the shear components of the material model correctly a plane strain simple shear deformation was modelled in \texttt{Abaqus} and compared with the analytical solution. The deformation gradient for this motion is 
\begin{equation}
\bm{F} = \left[\begin{array}{c c c}
1 & 0 & 0 \\
c & 1 & 0\\
0 & 0 & 1
\end{array} \right].
\end{equation}
Note that this motion is inherently volume preserving so one need not determine the hydrostatic pressure as it will always be zero.  
Hence, the stress of the material under this motion can be captured by
\begin{equation}
\bm{\sigma} = \dfrac{1}{J} \sum_{i=1}^3  \left(\sum_{p=1}^N\mu_p\left[\hat{\lambda}_i^{\alpha_p} - \dfrac{1}{3}\sum_{j=1}^3\hat{\lambda}_j^{\alpha_p}\right] \right)\boldsymbol{n}_i\otimes\boldsymbol{n}_i + 2J^{-1}(\hat{I}_4-1)k_1e^{k_2(\hat{I}_4-1)^2}(\bm{a}\otimes \bm{a} - \dfrac{1}{3}\hat{I}_4\bm{I}).
\end{equation}
Evaluating the Left Cauchy-Green tensor of this motion yields
\begin{equation}
\bm{b} = \left[\begin{array}{c c c}
1 	& c & 0 \\
c & 1 + c^2 & 0\\
0 & 0 & 1
\end{array} \right].
\end{equation}
This can then be used to evaluate the principal stretches,
\begin{equation}
\begin{gathered}
\lambda_1^2 = 1 + \dfrac{1}{2}c(c + \sqrt{c^2+4}),  \\
\lambda_2^2 = 1, \\
\lambda_3^2 = 1 + \dfrac{1}{2}c(c - \sqrt{c^2+4})  
\end{gathered}
\end{equation}
and principal directions in the current configuration
\begin{equation}
\begin{gathered}
\bm{n}_1 = \left[\begin{array}{c}
\frac{1}{2}(\sqrt{c^2+4}-c) \\ 1 \\ 0
\end{array} \right], \text{		}
\bm{n}_2 = \left[\begin{array}{c}
0 \\ 0 \\ 1
\end{array} \right], \text{		}
\bm{n}_3 = \left[\begin{array}{c }
-\frac{1}{2}(\sqrt{c^2+4}+c) \\ 1 \\ 0
\end{array} \right].
\end{gathered}
\end{equation}
It is clear from the eigen vectors that $\sigma_{13} = \sigma_{23} = \sigma_{31} = \sigma_{32} = 0$. Hence, it is convenient to define the vector $\bm{\sigma}_{other}$ as 
\begin{equation}
\bm{\sigma}_{other} = \left[ \begin{array}{c c}
\sigma_{13}& \sigma_{23}
\end{array} \right]^T.
\end{equation}
The tests were run with fibre directions of $\bm{a}_0 = \left[ \begin{array}{c c c}
1 & 0 & 0
\end{array} \right]^T$ and $\bm{a}_0 = \left[ \begin{array}{c c c}
0 & 1 & 0
\end{array} \right]^T$, respectively. A similar process to the last case was followed out where a \texttt{Python} script was written to automatically create and run multiple simulations in \texttt{Abaqus} with various values of $c$, a second script was written to convert the necessary data into easily accessible text files and a final script plotted these results against the analytical solution for the problem. The results for the cases with fibre directions of $\bm{a}_0 = \left[\begin{array}{c c c}
1 & 0 & 0
\end{array} \right]^T$ and $\bm{a}_0 = \left[ \begin{array}{c c c}
0 & 1 & 0
\end{array} \right]^T$ can be seen in Figures \ref{fig:par_shear} and \ref{fig:per_shear}, respectively.
\begin{figure}[!htb]
\centering
\includegraphics[width=0.8\linewidth]{par_shear}
\caption{Comparison of results of finite shear loading with a fibre direction of $\bm{a}_0 = \left[1,0,0 \right]^T$ simulated in \texttt{Abaqus} compared against the analytical solution.}
\label{fig:par_shear}
\end{figure}

\begin{figure}[!htb]
\centering
\includegraphics[width=0.8\linewidth]{per_shear}
\caption{Comparison of results of finite shear loading with a fibre direction of $\bm{a}_0 = \left[0,1,0 \right]^T$ simulated in \texttt{Abaqus} compared against the analytical solution.}
\label{fig:per_shear}
\end{figure}

\pagebreak
\section{Validation of Overall Model}

\begin{table}[!htb]
\centering
\begin{tabular}{c | c | c | c | c | c }
depth & width & thickness & protrusion length & protrusion width & collagen width\\
\hline \hline
3 & 8 & 3 & 2 & 0.3 & 0.1
\end{tabular}
\end{table}

\subsection{Three Point Bend Test}

\subsection{Stress Distribution in Protrusions}

\pagebreak
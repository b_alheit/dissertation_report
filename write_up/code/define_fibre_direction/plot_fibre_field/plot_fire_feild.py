import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d

filter_number = 100

protrusion_length = 8.0
protrusion_width = 4.
path_height = protrusion_length/2.0
n_surface = 50
gamma = 0.25
theta = np.pi * gamma

def surface(y, z):
    return -(path_height/2) * (np.cos(np.pi*y/protrusion_width - np.pi) + np.cos(np.pi*z/protrusion_width - np.pi))

def perp_xyz(position):
    x, y, z = position[0], position[1], position[2]
    normal_y = np.pi*path_height/(2*protrusion_width)*np.sin(y*np.pi/protrusion_width - np.pi)
    normal_z = np.pi*path_height/(2*protrusion_width)*np.sin(z*np.pi/protrusion_width - np.pi)
    normal = np.array([-1, normal_y, normal_z])
    # normal = np.array([(normal_y**2 + normal_z**2)**0.5, normal_y, normal_z])
    normal /= np.linalg.norm(normal)
    return -normal

def par_xyz(position):
    x, y, z = position[0], position[1], position[2]
    normal_y = np.pi*path_height/(2*protrusion_width)*np.sin(y*np.pi/protrusion_width - np.pi)
    normal_z = np.pi*path_height/(2*protrusion_width)*np.sin(z*np.pi/protrusion_width - np.pi)
    # normal = np.array([-1, normal_y, normal_z])
    normal = np.array([(normal_y**2 + normal_z**2)**0.5, normal_y, normal_z])
    normal /= np.linalg.norm(normal)
    return normal


positions = np.loadtxt("../output_data/positions.dat")
positions = positions[::filter_number, :]
# positions = positions[(positions[:, 1] < 4) & (positions[:, 1] > -4) & (positions[:, 2] > 0) & (positions[:, 2] < 5)]
n_plot = np.alen(positions[:, 0])
U = np.array([perp_xyz(position) for position in positions])

y = np.linspace(-4, 4, n_surface)
z = np.linspace(4, 12, n_surface)

Y, Z = np.meshgrid(y, z)
X = surface(Y, Z)

U_perp = np.zeros([n_surface**2, 3])
U_perp_pos = np.zeros([n_surface**2, 3])
k = 0
for i in range(0, n_surface, 5):
    for j in range(0, n_surface, 5):
        U_perp[k, :] = perp_xyz([0, Y[i, j], Z[i, j]])
        U_perp_pos[k, :] = [X[i, j], Y[i, j], Z[i, j]]
        k += 1

U_par = np.zeros([n_surface**2, 3])
U_par_pos = np.zeros([n_surface**2, 3])
k = 0
for i in range(0, n_surface, 5):
    for j in range(0, n_surface, 5):
        U_par[k, :] = par_xyz([0, Y[i, j], Z[i, j]])
        U_par_pos[k, :] = [X[i, j], Y[i, j], Z[i, j]]
        k += 1


# U = np.array([perp_xyz([0, Y[i, j], Z[i, j]]) for i, j in zip((range(n_surface),range(n_surface)))])
U_f = U_par*np.cos(theta) + U_perp*np.sin(theta)

fig = plt.figure()
ax = plt.axes(projection='3d')

ax.plot_surface(X, Z, Y, rstride=1, cstride=1,
                cmap='viridis', edgecolor='none')

# ax.scatter3D(positions[:, 0], positions[:, 2], positions[:, 1], color="green", s=1)
# ax.contour(positions[:, 0], positions[:, 2], positions[:, 1])
unit_positions = positions/np.outer((positions[:, 0]**2 + positions[:, 1]**2 + positions[:, 2]**2)**(0.5), np.ones(3))
# ax.quiver(positions[:, 0], positions[:, 2], positions[:, 1], U[:, 0], U[:, 2], U[:, 1])
# U_perp_pos[:, 0] += 0.1
# U_par_pos[:, 0] += 0.1
ax.quiver(U_perp_pos[:, 0], U_perp_pos[:, 2], U_perp_pos[:, 1], U_perp[:, 0], U_perp[:, 2], U_perp[:, 1])
ax.quiver(U_par_pos[:, 0], U_par_pos[:, 2], U_par_pos[:, 1], U_par[:, 0], U_par[:, 2], U_par[:, 1], color="red")
ax.quiver(U_par_pos[:, 0], U_par_pos[:, 2], U_par_pos[:, 1], U_f[:, 0], U_f[:, 2], U_f[:, 1], color="green")
plt.xlabel("x")
plt.ylabel("z")
plt.show()
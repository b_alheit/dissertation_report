# -*- coding: mbcs -*-
# Do not delete the following import lines
from abaqus import *
from abaqusConstants import *
from helpful_functions import *
import __main__
import math
import numpy as np

def Macro2():
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior
    # session.viewports['Viewport: 1'].assemblyDisplay.setValues(loads=OFF, bcs=OFF,
    #     predefinedFields=OFF, connectors=OFF)
    # mdb.jobs['Job-1'].writeInput(consistencyChecking=OFF)
    # mdb.jobs['Job-1'].submit(consistencyChecking=OFF)
    # session.mdbData.summary()
    o3 = session.openOdb(
        name='/home/cerecam/Benjamin_Alheit/Projects/masters-disertation/dissertation_report/write_up/code/define_fibre_direction/new-geometry-load-test.odb')
    session.viewports['Viewport: 1'].setValues(displayedObject=o3)
    lastFrame = o3.steps['coords'].frames[-1]
    # For each field output value in the last frame,
    # print the name, description, and type members.
    for f in lastFrame.fieldOutputs.values():
        print(f.name, ':', f.description)
        print('Type: ', f.type)
        # For each location value, print the position.
        for loc in f.locations:
            print('Position:',loc.position)
        print()
    sv4=lastFrame.fieldOutputs['SDV1']
    sv5=lastFrame.fieldOutputs['SDV1']
    sv6=lastFrame.fieldOutputs['SDV1']
    fieldValues=displacement.values
    print("Field Values")
    print(fieldValues[0])
    print( )
    print(fieldValues[0].data)
    # field = frame.fieldOutputs['COORD'].getSubset(position=INTEGRATION_POINT)

    # for v in fieldValues:
    #     print(v)
    #     print()
    #     print(v.data)

def create_fd_field_data():
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior
    # session.viewports['Viewport: 1'].assemblyDisplay.setValues(loads=OFF, bcs=OFF,
    #     predefinedFields=OFF, connectors=OFF)
    # mdb.jobs['Job-1'].writeInput(consistencyChecking=OFF)
    # mdb.jobs['Job-1'].submit(consistencyChecking=OFF)
    # session.mdbData.summary()
    o3 = session.openOdb(
        name='/home/cerecam/Benjamin_Alheit/Projects/masters-disertation/dissertation_report/write_up/code/define_fibre_direction/new-geometry-load-test-hpc2.odb', readOnly=False)
    lastFrame = o3.steps['coords'].frames[-1]

    # print(collagen_instance)
    # n_pts = np.alen(lastFrame.fieldOutputs['SDV4'].values)
    # print()
    # print(lastFrame.fieldOutputs['SDV4'].values[0])
    # print("Data")
    # print(lastFrame.fieldOutputs['SDV4'].values[0].data)
    # print(lastFrame.fieldOutputs['SDV4'].values[1])
    # print("Data")
    # print(lastFrame.fieldOutputs['SDV4'].values[1].data)
    # print("last element")
    # print(lastFrame.fieldOutputs['SDV4'].values[-1].elementLabel)
    # FB = np.zeros([, 3])

    collagen_instance = lastFrame.fieldOutputs['SDV1'].values[0].instance
    sv1=lastFrame.fieldOutputs['SDV1'].values
    sv2=lastFrame.fieldOutputs['SDV2'].values
    sv3=lastFrame.fieldOutputs['SDV3'].values
    n_points = np.alen(sv1)
    n_elements = lastFrame.fieldOutputs['SDV1'].values[-1].elementLabel
    positions = np.array([(sv1[i].data, sv2[i].data, sv3[i].data) for i in range(n_points)])
    # print("Positions")
    # print(positions)
    positions = tuple(map(tuple, positions))
    # print("tuple positions")
    # print(positions)

    elementLabels = tuple(range(1, 1 + n_elements))
    # print("Element labels")
    # print(elementLabels)

    FD = lastFrame.FieldOutput(name='FD', description='Fibre Direction', type=VECTOR, validInvariants=(MAGNITUDE,),
    componentLabels=('x comp', 'y comp', 'z comp'))

    FD.addData(position=INTEGRATION_POINT, instance=collagen_instance, labels=elementLabels, data=positions)

    o3.save()
    o3.close()




    # np.savetxt("./output_data/positions.dat", positions)
    # field = frame.fieldOutputs['COORD'].getSubset(position=INTEGRATION_POINT)

    # for v in fieldValues:
    #     print(v)
    #     print()
    #     print(v.data)


def convert_sv_to_position_file():
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior
    # session.viewports['Viewport: 1'].assemblyDisplay.setValues(loads=OFF, bcs=OFF,
    #     predefinedFields=OFF, connectors=OFF)
    # mdb.jobs['Job-1'].writeInput(consistencyChecking=OFF)
    # mdb.jobs['Job-1'].submit(consistencyChecking=OFF)
    # session.mdbData.summary()
    o3 = session.openOdb(
        name='/home/cerecam/Benjamin_Alheit/Projects/masters-disertation/dissertation_report/write_up/code/define_fibre_direction/new-geometry-load-test-hpc1.odb')
    # session.viewports['Viewport: 1'].setValues(displayedObject=o3)
    lastFrame = o3.steps['coords'].frames[-1]
    # For each field output value in the last frame,
    # print the name, description, and type members.
    # for f in lastFrame.fieldOutputs.values():
    #     print(f.name, ':', f.description)
    #     print('Type: ', f.type)
    #     # For each location value, print the position.
    #     for loc in f.locations:
    #         print('Position:',loc.position)
    #     print()
    sv4=lastFrame.fieldOutputs['SDV4'].values
    sv5=lastFrame.fieldOutputs['SDV5'].values
    sv6=lastFrame.fieldOutputs['SDV6'].values
    # sv4=sv4.values
    # sv5=sv5.values
    # sv6=sv6.values
    # print("Field Values")
    # print(fieldValues[0])
    # print( )
    # print(fieldValues[0].data)
    n_points = np.alen(sv4)
    positions = np.array([[sv4[i].data, sv5[i].data, sv6[i].data] for i in range(n_points)])
    # print("positions")
    # print(positions)
    np.savetxt("./output_data/positions.dat", positions)
    # field = frame.fieldOutputs['COORD'].getSubset(position=INTEGRATION_POINT)

    # for v in fieldValues:
    #     print(v)
    #     print()
    #     print(v.data)


def new_bone_args(activate_breakpoints,
    part_name,
    depth_of_section,
    horizontal_section_length,
    radius_of_curvature,
    right_thickness,
    left_thickness,
    protrusion_length):

    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior

    # activate_breakpoints = False
    # part_name = "bone2"
    # depth_of_section = 50.0
    # horizontal_section_length = 50.0
    # radius_of_curvature = 100.0
    # right_thickness = 15.0
    # left_thickness = 10.0
    # protrusion_length = 15.0

    # activate_breakpoints = False
    # part_name = "bone1"
    # depth_of_section = 60.0
    # horizontal_section_length = 75.0
    # radius_of_curvature = 180.0
    # right_thickness = 10.0
    # left_thickness = 5.0
    # protrusion_length = 25.0

    print(part_name)
    s1 = mdb.models['Model-1'].ConstrainedSketch(name='__profile__', sheetSize=200.0)
    g, v, d, c = s1.geometry, s1.vertices, s1.dimensions, s1.constraints
    s1.setPrimaryObject(option=STANDALONE)

    # Creating y-axis
    y_axis = s1.ConstructionLine(point1=(0.0, 0.0), point2=(0.0, 10.0))  # g2
    y_axis_fix = s1.FixedConstraint(entity=y_axis)

    # Creating x-axis
    x_axis = s1.ConstructionLine(point1=(0.0, 0.0), point2=(10.0, 0.0))  # g3
    x_axis_fix = s1.FixedConstraint(entity=x_axis)

    # Creating line parallel to x-axis
    cline_x_axis_parallel = s1.ConstructionLine(point1=(0.0, -10.0), point2=(5.0, -10.0))  # g4
    s1.ParallelConstraint(entity1=cline_x_axis_parallel, entity2=x_axis)
    s1.DistanceDimension(entity1=cline_x_axis_parallel, entity2=x_axis, textPoint=(33.7514762878418, -3.71352958679199), value=right_thickness)

    arc_top, v_arc_top_center, v_arc_top_point1, v_arc_top_point2  = make_ArcByCenterEnds(s1, center=(-5.0, -28.75), point1=(-5.0, -5.0), point2=(-28.75, -23.75), direction=COUNTERCLOCKWISE)
    s1.CoincidentConstraint(entity1=v_arc_top_center, entity2=y_axis)
    s1.CoincidentConstraint(entity1=v_arc_top_point1, entity2=x_axis)
    s1.CoincidentConstraint(entity1=v_arc_top_point1, entity2=y_axis)
    s1.HorizontalDimension(vertex1=v[0], vertex2=v[1], textPoint=(-10.8330364227295, 18.8702774047852), value=horizontal_section_length)
    s1.VerticalDimension(vertex1=v[2], vertex2=v[0], textPoint=(64.4481735229492, -15.0), value=radius_of_curvature)

    arc_bottom, v_arc_bottom_center, v_arc_bottom_point1, v_arc_bottom_point2  = make_ArcByCenterEnds(s1, center=(-5.0, -35.0), point1=(-5.0, -20.0), point2=(-21.25, -32.5), direction=COUNTERCLOCKWISE)
    s1.CoincidentConstraint(entity1=v_arc_bottom_center, entity2=y_axis)
    s1.CoincidentConstraint(entity1=v_arc_bottom_point1, entity2=y_axis)
    s1.CoincidentConstraint(entity1=v_arc_bottom_point1, entity2=cline_x_axis_parallel)

    line_left, line_left_point1, line_left_point2 = make_Line(s1, point1=(-35.0, -25.0), point2=(-25.0, -32.5), message="line left")
    s1.CoincidentConstraint(entity1=line_left_point1, entity2=v_arc_top_point2)
    s1.CoincidentConstraint(entity1=line_left_point2, entity2=v_arc_bottom_point2)
    s1.PerpendicularConstraint(entity1=line_left, entity2=arc_bottom)
    s1.ObliqueDimension(vertex1=line_left_point1, vertex2=line_left_point2, textPoint=(-46.3366470336914, -46.5077590942383), value=left_thickness)

    line_bottom, dummy, v_line_bottom_point2 = make_Line(s1, point1=(0.0, -right_thickness), point2=(10.0, -right_thickness), message="line bottom")
    break_point(activate_breakpoints, "big shaq1")
    s1.CoincidentConstraint(entity1=line_bottom, entity2=cline_x_axis_parallel)
    break_point(activate_breakpoints, "big shaq1.1")
    line_right, dummy, dummy = make_Line(s1, point1=(10.0, -right_thickness), point2=(10.0, 0.0), message="line right")
    break_point(activate_breakpoints, "big shaq2")
    s1.VerticalConstraint(entity=line_right, addUndoState=False)
    break_point(activate_breakpoints, "big shaq2.1")
    line_top, dummy, dummy = make_Line(s1, point1=(10.0, 0.0), point2=(0.0, 0.0), message="line top")
    break_point(activate_breakpoints, "big shaq3")
    s1.CoincidentConstraint(entity1=line_top, entity2=x_axis)
    break_point(activate_breakpoints, "big shaq3.1")

    s1.HorizontalDimension(vertex1=v_arc_bottom_point1, vertex2=v_line_bottom_point2, textPoint=(6.18564033508301, -22.5884265899658), value=protrusion_length)
    break_point(activate_breakpoints, "big shaq9")

    session.viewports['Viewport: 1'].view.setValues(nearPlane=163.685, farPlane=213.438, width=253.528, height=138.692, cameraPosition=(-18.3291, 29.1575, 188.562), cameraTarget=(-18.3291, 29.1575, 0))
    p = mdb.models['Model-1'].Part(name=part_name, dimensionality=THREE_D, type=DEFORMABLE_BODY)
    p = mdb.models['Model-1'].parts[part_name]
    p.BaseSolidExtrude(sketch=s1, depth=depth_of_section)
    s1.unsetPrimaryObject()
    p = mdb.models['Model-1'].parts[part_name]
    session.viewports['Viewport: 1'].setValues(displayedObject=p)
    del mdb.models['Model-1'].sketches['__profile__']

def path_points(mult, gap_mult, exp, dist, depth_of_section, n_path_points, path_func, path_theta_func, omega, fil_mult):

    def bottom_adjust(x, y, theta):
        angle_rad = math.pi/2 + math.pi/4 * math.cos(theta-math.pi/2)
        X = x - mult*dist * math.copysign(math.cos(angle_rad) ** exp, math.cos(angle_rad))
        Y = y + mult*dist * math.sin(angle_rad)
        return X, Y

    T = math.pi/(omega)
    n_s_pts = int(depth_of_section/(2.0*T))
    if n_s_pts == 0:
        n_s_pts = 1
    x_peak_right = T * n_s_pts
    if math.fabs(x_peak_right-depth_of_section/2) < 1.0e-10:
        n_s_pts -= 1
        x_peak_right = T * n_s_pts
    r = gap_mult* depth_of_section/2 - x_peak_right

    print('*************************')
    print('d', depth_of_section/2)
    print('x_peak_right ', x_peak_right)
    print('n_s_pts', n_s_pts)
    print('T', T)
    print('')

    y_peak = path_func(x_peak_right)
    theta = path_theta_func(x_peak_right)
    X, y_peak = bottom_adjust(x_peak_right, y_peak, theta)


    y_peak-= fil_mult*r
    x_peak_left = -x_peak_right
    path = []

    dx_int = (depth_of_section - (2*r)) / (int((n_path_points-1.0) * (depth_of_section - (2*r))/depth_of_section)+1.0)
    dx_int_r = r / (int((n_path_points-1.0) * (2*r)/depth_of_section)+1.0)
    x = -gap_mult* depth_of_section/2
    while x <= x_peak_left:
        y = y_peak + fil_mult*(r**2 - (x-x_peak_left)**2)**0.5
        path.append((x, y))
        x += dx_int_r

    x = x_peak_left + dx_int

    while x <= x_peak_right:
        y = path_func(x)
        theta = path_theta_func(x)
        X, Y = bottom_adjust(x, y, theta)
        path.append((X, Y))
        x += dx_int

    x = x_peak_right

    while x <= gap_mult* depth_of_section/2:
        y = y_peak + fil_mult *(r**2 - (x-x_peak_right)**2)**0.5
        path.append((x, y))
        x += dx_int_r

    path = tuple(path)

    return path

def path_points_straight(mult, gap_mult, exp, dist, depth_of_section, n_path_points, path_func, path_theta_func, omega, fil_mult):

    def bottom_adjust(x, y, theta):
        angle_rad = math.pi/2 + math.pi/4 * math.cos(theta-math.pi/2)
        X = x - mult*dist * math.copysign(math.cos(angle_rad) ** exp, math.cos(angle_rad))
        Y = y + mult*dist * math.sin(angle_rad)
        return X, Y

    T = math.pi/(omega)
    n_s_pts = int(depth_of_section/(2.0*T))
    if n_s_pts == 0:
        n_s_pts = 1
    x_peak_right = T * n_s_pts
    if math.fabs(x_peak_right-depth_of_section/2) < 1.0e-10:
        n_s_pts -= 1
        x_peak_right = T * n_s_pts
    r = gap_mult* depth_of_section/2 - x_peak_right

    print('*************************')
    print('d', depth_of_section/2)
    print('x_peak_right ', x_peak_right)
    print('n_s_pts', n_s_pts)
    print('T', T)
    print('')

    y_peak = path_func(x_peak_right)
    theta = path_theta_func(x_peak_right)
    X, y_peak = bottom_adjust(x_peak_right, y_peak, theta)
    y_straight = y_peak

    y_peak-= fil_mult*r
    x_peak_left = -x_peak_right
    path = []

    dx_int = (depth_of_section - (2*r)) / (int((n_path_points-1.0) * (depth_of_section - (2*r))/depth_of_section)+1.0)
    dx_int_r = r / (int((n_path_points-1.0) * (2*r)/depth_of_section)+1.0)
    x = -1.1*gap_mult* depth_of_section/2
    while x <= x_peak_left:
        # y = y_peak + fil_mult*(r**2 - (x-x_peak_left)**2)**0.5
        y = y_straight
        path.append((x, y))
        x += dx_int_r

    x = x_peak_left + dx_int

    while x <= x_peak_right:
        y = path_func(x)
        theta = path_theta_func(x)
        X, Y = bottom_adjust(x, y, theta)
        path.append((X, Y))
        x += dx_int

    x = x_peak_right

    while x <= gap_mult* depth_of_section/2:
        y = y_peak + fil_mult *(r**2 - (x-x_peak_right)**2)**0.5
        path.append((x, y))
        x += dx_int_r

    path = tuple(path)

    return path

def path_points1(mult, gap_mult, exp, dist, depth_of_section, n_path_points, path_func, path_theta_func, om):


    def bottom_adjust(x, y, theta):
        angle_rad = math.pi/2 + math.pi/4 * math.cos(theta-math.pi/2)
        X = x - mult*dist * math.copysign(math.cos(angle_rad) ** exp, math.cos(angle_rad))
        Y = y + mult*dist * math.sin(angle_rad)
        return X, Y

    path = []

    dx = depth_of_section / (n_path_points-1.0)
    x = 100*dx-depth_of_section/2
    X = 1
    while X >= -gap_mult*depth_of_section/2:
        x -= dx
        y = path_func(x)
        theta = path_theta_func(x)
        X, Y = bottom_adjust(x, y, theta)


    path.append((X, Y))

    while X <= gap_mult*depth_of_section/2:
        x += dx
        y = path_func(x)
        theta = path_theta_func(x)
        X, Y = bottom_adjust(x, y, theta)

        path.append((X, Y))

    path = tuple(path)

    return path


def all_together():
    activate_breakpoints = False
    left_bone_name = 'left-bone'
    right_bone_name = 'right-bone'
    col_part_name = 'collagen'
    keratin_part_name = 'keratin'
    depth_of_section = 20.0
    horizontal_section_length = 25.0
    radius_of_curvature = 120.0
    right_thickness = 10.0
    left_thickness = 10.0
    protrusion_length = 15.0
    protrusion_width = 2.2
    collagen_thickness = 1.5
    n_points = 180
    n_path_points = n_points
    collagen_top_thickness = 1.0
    keratin_thickness = 1.0
    collagen_translate = 2.5
    pre_cut_col_thickness = 3 * (protrusion_length + collagen_thickness + collagen_top_thickness)
    flat_length = protrusion_length/2 + collagen_thickness + collagen_translate

    # n_teeth = int(right_thickness/protrusion_width)
    # print('n_teeth')
    # print(n_teeth)
    # protrusion_width = right_thickness/(2*n_teeth)

    # # sketching path
    path_height = protrusion_length/2.0
    dist = collagen_thickness/2.0
    #
    # path_theta_func = lambda x: x*math.pi/protrusion_width - math.pi
    # bone_func = lambda x: path_height/2.0*math.cos(path_theta_func(x)) - path_height/2.0
    # col_func = lambda x: bone_func(x) +  path_height
    #
    # path_func = lambda x: path_height/2.0*math.cos(path_theta_func(x)) + path_height/2.0
    #
    # # break_point(True, 'hi')
    # bone_path = path_points(1, 1, dist, depth_of_section, n_path_points, bone_func, path_theta_func)
    # bone_cut = path_points(1, 1.25, dist, right_thickness, n_path_points, bone_func, path_theta_func)
    # left_col_path = path_points(1, 1, dist, depth_of_section, n_path_points, col_func, path_theta_func)
    # right_col_path = path_points(-1, 1, dist, depth_of_section, n_path_points, col_func, path_theta_func)
    # left_col_cut = path_points(1, 1.25, dist, right_thickness, n_path_points, col_func, path_theta_func)
    # right_col_cut = path_points(-1, 1.25, dist, right_thickness, n_path_points, col_func, path_theta_func)
    # right_bone_path = path_points(-1, 1, dist, depth_of_section, n_path_points, col_func, path_theta_func)
    # right_bone_cut = path_points(-1, 1.25, dist, right_thickness, n_path_points, col_func, path_theta_func)
    omega = math.pi/protrusion_width
    path_theta_func = lambda x: x*omega - math.pi
    bone_func = lambda x: path_height/2.0*math.cos(path_theta_func(x)) - path_height/2.0
    col_func = lambda x: bone_func(x) +  path_height

    path_func = lambda x: path_height/2.0*math.cos(path_theta_func(x)) + path_height/2.0

    exp  = 3.0
    mult_path = 1
    mult_cut = 1

    gap_mult_path = 1.01
    gap_mult_cut = 1.01
    right_filet_mult = 1.0
    left_filet_mult = -1.0
    path_dist = 0

    # break_point(True, 'hi')
    # bone_cut = path_points(mult_cut, gap_mult_cut, exp, dist, right_thickness, n_path_points, bone_func, path_theta_func, omega, left_filet_mult)
    # left_col_cut = path_points(mult_cut, gap_mult_cut, exp, dist, right_thickness, n_path_points, col_func, path_theta_func, omega, left_filet_mult)
    # right_col_cut = path_points(-mult_cut, gap_mult_cut, exp, dist, right_thickness, n_path_points, col_func, path_theta_func, omega, right_filet_mult)
    # right_bone_cut = path_points(-mult_cut, gap_mult_cut, exp, dist, right_thickness, n_path_points, col_func, path_theta_func, omega, right_filet_mult)

    bone_cut = path_points_straight(mult_cut, gap_mult_cut, exp, dist, right_thickness, n_path_points, bone_func, path_theta_func, omega, left_filet_mult)
    left_col_cut = path_points_straight(mult_cut, gap_mult_cut, exp, dist, right_thickness, n_path_points, col_func, path_theta_func, omega, left_filet_mult)
    right_col_cut = path_points_straight(-mult_cut, gap_mult_cut, exp, dist, right_thickness, n_path_points, col_func, path_theta_func, omega, right_filet_mult)
    right_bone_cut = path_points_straight(-mult_cut, gap_mult_cut, exp, dist, right_thickness, n_path_points, col_func, path_theta_func, omega, right_filet_mult)


        # break_point(True, 'hi')


    bone_path = path_points1(mult_path, gap_mult_path, exp, path_dist, depth_of_section, n_path_points, bone_func, path_theta_func, omega)
    # bone_cut = path_points1(mult_cut, gap_mult_cut, exp, dist, right_thickness, n_path_points, bone_func, path_theta_func, omega)
    # # left_col_path = path_points(1, 1, exp, dist, depth_of_section, n_path_points, col_func, path_theta_func)
    left_col_path = path_points1(mult_path, gap_mult_path, exp, path_dist, depth_of_section, n_path_points, col_func, path_theta_func, omega)
    # # right_col_path = path_points(-1, 1, exp, dist, depth_of_section, n_path_points, col_func, path_theta_func)
    right_col_path = path_points1(-mult_path, gap_mult_path, exp, path_dist, depth_of_section, n_path_points, col_func, path_theta_func, omega)
    # left_col_cut = path_points1(mult_cut, gap_mult_cut, exp, dist, right_thickness, n_path_points, col_func, path_theta_func, omega)
    # right_col_cut = path_points1(-mult_cut, gap_mult_cut, exp, dist, right_thickness, n_path_points, col_func, path_theta_func, omega)
    right_bone_path = path_points1(-mult_path, gap_mult_path, exp, path_dist, depth_of_section, n_path_points, col_func, path_theta_func, omega)
    # right_bone_cut = path_points1(-mult_cut, gap_mult_cut, exp, dist, right_thickness, n_path_points, col_func, path_theta_func, omega)


    new_bone_args(activate_breakpoints,
        left_bone_name,
        depth_of_section,
        horizontal_section_length,
        radius_of_curvature,
        right_thickness,
        left_thickness,
        protrusion_length)
    create_left_bone_protrusions_args(left_bone_name, protrusion_length,
        protrusion_width, n_points, depth_of_section, right_thickness,
        protrusion_length, collagen_thickness, bone_path, bone_cut, False)

    new_bone_args(activate_breakpoints,
        right_bone_name,
        depth_of_section,
        horizontal_section_length,
        radius_of_curvature,
        right_thickness,
        left_thickness,
        protrusion_length)

    create_right_bone_protrusions_args(right_bone_name, protrusion_length,
    protrusion_width, n_points, depth_of_section, right_thickness,
    protrusion_length, collagen_thickness, right_bone_path, right_bone_cut, False)

    create_col_block_args(col_part_name, right_thickness, pre_cut_col_thickness,
        depth_of_section)
    col_block_right_cut_args(col_part_name, protrusion_length, protrusion_width,
        n_path_points, depth_of_section, right_thickness, protrusion_length, collagen_thickness, pre_cut_col_thickness, collagen_translate, right_col_path, right_col_cut, False)
    col_block_left_cut_args(col_part_name, protrusion_length, protrusion_width,
        n_path_points, depth_of_section, right_thickness, protrusion_length, collagen_thickness, pre_cut_col_thickness, collagen_translate, left_col_path, left_col_cut, False)
    collagen_top_args(col_part_name, depth_of_section, collagen_top_thickness,
        radius_of_curvature, horizontal_section_length, right_thickness, flat_length, protrusion_length, False)

    make_keratin(keratin_part_name, horizontal_section_length, depth_of_section, radius_of_curvature+collagen_top_thickness, flat_length, keratin_thickness, keratin_thickness, False)



def create_left_bone_protrusions_args(part_name, pro_height, pro_thick, n_points_path, depth_of_section, right_thickness, protrusion_length, col_thick, bone_path, bone_cut, activate_breakpoints=False):
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior

    print(part_name)
    dist = col_thick/2.0

    break_point(activate_breakpoints, "1")
    p = mdb.models['Model-1'].parts[part_name]
    f, e = p.faces, p.edges
    t = p.MakeSketchTransform(sketchPlane=f[5], sketchUpEdge=e[17],
        sketchPlaneSide=SIDE1, sketchOrientation=RIGHT, origin=(0.0, 0.0,
        depth_of_section/2))
    s = mdb.models['Model-1'].ConstrainedSketch(name='__sweep__', sheetSize=104.4, gridSpacing=2.61, transform=t)
    g, v, d, c = s.geometry, s.vertices, s.dimensions, s.constraints
    s.setPrimaryObject(option=SUPERIMPOSE)

    break_point(activate_breakpoints, "2")
    p = mdb.models['Model-1'].parts[part_name]
    p.projectReferencesOntoSketch(sketch=s, filter=COPLANAR_EDGES)

    s.Spline(points=bone_path)

    break_point(activate_breakpoints, "3")
    s.unsetPrimaryObject()
    s.unsetPrimaryObject()

    p = mdb.models['Model-1'].parts[part_name]

    f1, e1 = p.faces, p.edges
    t = p.MakeSketchTransform(sketchPlane=f1[6], sketchUpEdge=e1[16],
        sketchPlaneSide=SIDE1, sketchOrientation=RIGHT, origin=(dist,
        -right_thickness/2.0, depth_of_section))
    s1 = mdb.models['Model-1'].ConstrainedSketch(name='__profile__',
        sheetSize=137.79, gridSpacing=3.44, transform=t)
    g1, v1, d1, c1 = s1.geometry, s1.vertices, s1.dimensions, s1.constraints
    s1.setPrimaryObject(option=SUPERIMPOSE)

    break_point(activate_breakpoints, "4")
    p = mdb.models['Model-1'].parts[part_name]

    p.projectReferencesOntoSketch(sketch=s1, filter=COPLANAR_EDGES)

    start_point, end_point  = bone_cut[0], bone_cut[len(bone_cut)-1]
    s1.Spline(points=bone_cut)

    break_point(activate_breakpoints, "4")

    point1 = start_point
    point2 = (start_point[0], start_point[1] - protrusion_length - pro_height)
    point3 = (end_point[0], point2[1])
    point4 = end_point

    break_point(activate_breakpoints, "4")
    s1.Line(point1=point1, point2=point2)
    s1.VerticalConstraint(entity=g1[9], addUndoState=False)
    break_point(activate_breakpoints, "4")
    s1.Line(point1=point2, point2=point3)
    s1.HorizontalConstraint(entity=g1[10], addUndoState=False)
    s1.PerpendicularConstraint(entity1=g1[9], entity2=g1[10], addUndoState=False)
    break_point(activate_breakpoints, "4")
    s1.Line(point1=point3, point2=point4)
    s1.VerticalConstraint(entity=g1[11], addUndoState=False)
    s1.PerpendicularConstraint(entity1=g1[10], entity2=g1[11], addUndoState=False)
    break_point(activate_breakpoints, "4")

    s1.unsetPrimaryObject()

    break_point(activate_breakpoints, "5")
    p = mdb.models['Model-1'].parts[part_name]
    break_point(activate_breakpoints, "5.1")

    f, e = p.faces, p.edges
    break_point(activate_breakpoints, "5.2")
    p.CutSweep(pathPlane=f[5], pathUpEdge=e[17], sketchPlane=f[6],
        sketchUpEdge=e[16], pathOrientation=RIGHT, path=s,
        sketchOrientation=RIGHT, profile=s1, profileNormal=ON)
    break_point(activate_breakpoints, "5.3")
    del mdb.models['Model-1'].sketches['__sweep__']
    del mdb.models['Model-1'].sketches['__profile__']


def create_right_bone_protrusions_args(part_name, pro_height, pro_thick, n_points_path, depth_of_section, right_thickness, protrusion_length, col_thick, right_bone_path, right_bone_cut, activate_breakpoints=False):
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior

    print(part_name)

    def top_adjust(x, y, theta):
        angle_rad = math.pi/2 + math.pi/4 * math.sin(theta-math.pi/2)
        X = x - dist * math.cos(angle_rad)
        Y = y + dist * math.sin(angle_rad)
        return X, Y

    break_point(activate_breakpoints, "1")
    p = mdb.models['Model-1'].parts[part_name]
    f, e = p.faces, p.edges
    t = p.MakeSketchTransform(sketchPlane=f[5], sketchUpEdge=e[16], sketchPlaneSide=SIDE1, sketchOrientation=RIGHT, origin=(0.0, 0.0, depth_of_section/2)) # Need to parameterise sketch height
    s = mdb.models['Model-1'].ConstrainedSketch(name='__sweep__', sheetSize=104.4, gridSpacing=2.61, transform=t)
    g, v, d, c = s.geometry, s.vertices, s.dimensions, s.constraints
    s.setPrimaryObject(option=SUPERIMPOSE)

    break_point(activate_breakpoints, "2")
    p = mdb.models['Model-1'].parts[part_name]
    p.projectReferencesOntoSketch(sketch=s, filter=COPLANAR_EDGES)

    # sketching path
    path_height = pro_height/2.0
    dist = col_thick/2.0

    break_point(activate_breakpoints, "in")
    s.Spline(points=right_bone_path)

    break_point(activate_breakpoints, "3")
    s.unsetPrimaryObject()
    s.unsetPrimaryObject()

    p = mdb.models['Model-1'].parts[part_name]

    f1, e1 = p.faces, p.edges
    t = p.MakeSketchTransform(sketchPlane=f1[7], sketchUpEdge=e1[17],
        sketchPlaneSide=SIDE1, sketchOrientation=RIGHT, origin=(dist,
        -right_thickness/2.0, 0.0))
    s1 = mdb.models['Model-1'].ConstrainedSketch(name='__profile__',
        sheetSize=137.79, gridSpacing=3.44, transform=t)
    g1, v1, d1, c1 = s1.geometry, s1.vertices, s1.dimensions, s1.constraints
    s1.setPrimaryObject(option=SUPERIMPOSE)

    break_point(False, "4")
    p = mdb.models['Model-1'].parts[part_name]

    p.projectReferencesOntoSketch(sketch=s1, filter=COPLANAR_EDGES)


    cut_points = right_bone_cut
    start_point, end_point  = cut_points[0], cut_points[len(cut_points)-1]

    break_point(activate_breakpoints, "4")

    s1.Spline(points=cut_points)

    break_point(activate_breakpoints, "4")

    point1 = start_point
    point2 = (start_point[0], start_point[1] + protrusion_length + pro_height)
    point3 = (end_point[0], point2[1])
    point4 = end_point

    s1.Line(point1=point1, point2=point2)
    s1.VerticalConstraint(entity=g1[9], addUndoState=False)
    s1.Line(point1=point2, point2=point3)
    s1.HorizontalConstraint(entity=g1[10], addUndoState=False)
    s1.PerpendicularConstraint(entity1=g1[9], entity2=g1[10], addUndoState=False)
    s1.Line(point1=point3, point2=point4)
    s1.VerticalConstraint(entity=g1[11], addUndoState=False)
    s1.PerpendicularConstraint(entity1=g1[10], entity2=g1[11], addUndoState=False)

    s1.unsetPrimaryObject()

    p = mdb.models['Model-1'].parts[part_name]

    f, e = p.faces, p.edges
    p.CutSweep(pathPlane=f[5], pathUpEdge=e[16], sketchPlane=f[7],
        sketchUpEdge=e[17], pathOrientation=RIGHT, path=s,
        sketchOrientation=RIGHT, profile=s1, profileNormal=ON)
    del mdb.models['Model-1'].sketches['__sweep__']
    del mdb.models['Model-1'].sketches['__profile__']


def create_col_block_args(part_name, right_bone_thickness, pre_cut_col_thickness, depth_of_section):
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior
    s = mdb.models['Model-1'].ConstrainedSketch(name='__profile__',
        sheetSize=200.0)
    g, v, d, c = s.geometry, s.vertices, s.dimensions, s.constraints
    s.setPrimaryObject(option=STANDALONE)
    s.rectangle(point1=(-pre_cut_col_thickness/2.0, -right_bone_thickness/2.0), point2=(pre_cut_col_thickness/2.0, right_bone_thickness/2.0))

    s.ObliqueDimension(vertex1=v[0], vertex2=v[1], textPoint=(-24.4589042663574, 14.7560958862305), value=right_bone_thickness)
    s.ObliqueDimension(vertex1=v[3], vertex2=v[0], textPoint=(10.9517517089844, -10.2439022064209), value=pre_cut_col_thickness)

    p = mdb.models['Model-1'].Part(name=part_name, dimensionality=THREE_D,
        type=DEFORMABLE_BODY)
    p = mdb.models['Model-1'].parts[part_name]
    p.BaseSolidExtrude(sketch=s, depth=depth_of_section)
    s.unsetPrimaryObject()
    p = mdb.models['Model-1'].parts[part_name]
    session.viewports['Viewport: 1'].setValues(displayedObject=p)
    del mdb.models['Model-1'].sketches['__profile__']


def col_block_right_cut_args(part_name, pro_height, pro_thick, n_points_path, depth_of_section, right_thickness, protrusion_length, col_thick, pre_cut_col_thickness, collagen_translate, right_col_path, right_col_cut, activate_breakpoints=False):
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior

    p = mdb.models['Model-1'].parts[part_name]
    f, e = p.faces, p.edges
    t = p.MakeSketchTransform(sketchPlane=f[1], sketchUpEdge=e[6],
        sketchPlaneSide=SIDE1, sketchOrientation=RIGHT, origin=(0.0, right_thickness/2,
        depth_of_section/2.0))
    s1 = mdb.models['Model-1'].ConstrainedSketch(name='__sweep__',
        sheetSize=231.51, gridSpacing=5.78, transform=t)
    g, v, d, c = s1.geometry, s1.vertices, s1.dimensions, s1.constraints
    s1.setPrimaryObject(option=SUPERIMPOSE)
    p = mdb.models['Model-1'].parts[part_name]
    p.projectReferencesOntoSketch(sketch=s1, filter=COPLANAR_EDGES)
    # sketching path
    path_height = pro_height/2.0
    dist = col_thick/2.0

    break_point(activate_breakpoints, "in")
    s1.Spline(points=right_col_path)
    break_point(activate_breakpoints, "1")
    s1.unsetPrimaryObject()
    s1.unsetPrimaryObject()

    p = mdb.models['Model-1'].parts[part_name]
    f1, e1 = p.faces, p.edges
    t = p.MakeSketchTransform(sketchPlane=f1[4], sketchUpEdge=e1[4],
        sketchPlaneSide=SIDE1, sketchOrientation=RIGHT, origin=(collagen_translate/2, 0.0,
        0.0))
    s = mdb.models['Model-1'].ConstrainedSketch(name='__profile__',
        sheetSize=231.51, gridSpacing=5.78, transform=t)
    g1, v1, d1, c1 = s.geometry, s.vertices, s.dimensions, s.constraints
    s.setPrimaryObject(option=SUPERIMPOSE)
    p = mdb.models['Model-1'].parts[part_name]
    p.projectReferencesOntoSketch(sketch=s, filter=COPLANAR_EDGES)
    break_point(activate_breakpoints, "4")

    cut_points = right_col_cut
    start_point, end_point  = cut_points[0], cut_points[len(cut_points)-1]
    #
    # print("cut points, soft code")
    # print(cut_points)

    break_point(activate_breakpoints, "4")

    s.Spline(points=cut_points)

    break_point(activate_breakpoints, "4")

    point1 = start_point
    point2 = (start_point[0], -(start_point[1] + pre_cut_col_thickness))
    point3 = (end_point[0], point2[1])
    point4 = end_point

    left_v = s.Line(point1=point1, point2=point2)
    s.VerticalConstraint(entity=left_v, addUndoState=False)
    h_line = s.Line(point1=point2, point2=point3)
    s.HorizontalConstraint(entity=h_line, addUndoState=False)
    s.PerpendicularConstraint(entity1=h_line, entity2=left_v, addUndoState=False)
    right_v = s.Line(point1=point3, point2=point4)
    s.VerticalConstraint(entity=right_v, addUndoState=False)
    s.PerpendicularConstraint(entity1=right_v, entity2=h_line, addUndoState=False)
    break_point(activate_breakpoints, "4")

    s.unsetPrimaryObject()

    p = mdb.models['Model-1'].parts[part_name]
    f, e = p.faces, p.edges
    p.CutSweep(pathPlane=f[1], pathUpEdge=e[6], sketchPlane=f[4],
        sketchUpEdge=e[4], pathOrientation=RIGHT, path=s1,
        sketchOrientation=RIGHT, profile=s, profileNormal=ON)

    del mdb.models['Model-1'].sketches['__sweep__']
    del mdb.models['Model-1'].sketches['__profile__']


def col_block_left_cut_args(part_name, pro_height, pro_thick, n_points_path, depth_of_section, right_thickness, protrusion_length, col_thick, pre_cut_col_thickness, collagen_translate, left_col_path, left_col_cut, activate_breakpoints=False):
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior
    break_point(activate_breakpoints, "1")

    p = mdb.models['Model-1'].parts[part_name]
    f, e = p.faces, p.edges
    t = p.MakeSketchTransform(sketchPlane=f[1], sketchUpEdge=e[4],
        sketchPlaneSide=SIDE1, sketchOrientation=RIGHT, origin=(0.0, right_thickness/2,
        depth_of_section/2.0))
    s1 = mdb.models['Model-1'].ConstrainedSketch(name='__sweep__',
        sheetSize=316.49, gridSpacing=7.91, transform=t)
    g, v, d, c = s1.geometry, s1.vertices, s1.dimensions, s1.constraints
    s1.setPrimaryObject(option=SUPERIMPOSE)
    p = mdb.models['Model-1'].parts[part_name]
    p.projectReferencesOntoSketch(sketch=s1, filter=COPLANAR_EDGES)

    break_point(activate_breakpoints, "Easy now")

    # sketching path
    path_height = pro_height/2.0
    dist = col_thick/2.0



    s1.Spline(points=left_col_path)

    break_point(activate_breakpoints, "1")


    s1.unsetPrimaryObject()
    s1.unsetPrimaryObject()
    session.viewports['Viewport: 1'].view.setValues(nearPlane=149.51,
        farPlane=212.448, width=69.029, height=35.6003, cameraPosition=(
        -13.7972, 167.129, 39.9679), cameraTarget=(-15.4906, -13.5907,
        30.4309))
    p = mdb.models['Model-1'].parts[part_name]
    f1, e1 = p.faces, p.edges
    t = p.MakeSketchTransform(sketchPlane=f1[5], sketchUpEdge=e1[6],
        sketchPlaneSide=SIDE1, sketchOrientation=RIGHT, origin=(-collagen_translate/2,
        0.0, 0.0))
    s = mdb.models['Model-1'].ConstrainedSketch(name='__profile__',
        sheetSize=304.28, gridSpacing=7.6, transform=t)
    g1, v1, d1, c1 = s.geometry, s.vertices, s.dimensions, s.constraints
    s.setPrimaryObject(option=SUPERIMPOSE)
    p = mdb.models['Model-1'].parts[part_name]
    p.projectReferencesOntoSketch(sketch=s, filter=COPLANAR_EDGES)

    break_point(activate_breakpoints, "4")


    cut_points = left_col_cut

    start_point, end_point  = cut_points[0], cut_points[len(cut_points)-1]

    break_point(activate_breakpoints, "4")

    s.Spline(points=cut_points)

    break_point(activate_breakpoints, "4")

    point1 = start_point
    point2 = (start_point[0], (start_point[1] + pre_cut_col_thickness))
    point3 = (end_point[0], point2[1])
    point4 = end_point

    break_point(activate_breakpoints, "4.1")
    left_v = s.Line(point1=point1, point2=point2)
    s.VerticalConstraint(entity=left_v, addUndoState=False)
    break_point(activate_breakpoints, "4")
    h_line = s.Line(point1=point2, point2=point3)
    s.HorizontalConstraint(entity=h_line, addUndoState=False)
    s.PerpendicularConstraint(entity1=h_line, entity2=left_v, addUndoState=False)
    break_point(activate_breakpoints, "4")
    right_v = s.Line(point1=point3, point2=point4)
    s.VerticalConstraint(entity=right_v, addUndoState=False)
    s.PerpendicularConstraint(entity1=right_v, entity2=h_line, addUndoState=False)
    break_point(activate_breakpoints, "4")

    break_point(activate_breakpoints, "2")

    s.unsetPrimaryObject()
    p = mdb.models['Model-1'].parts[part_name]
    f, e = p.faces, p.edges
    p.CutSweep(pathPlane=f[1], pathUpEdge=e[4], sketchPlane=f[5],
        sketchUpEdge=e[6], pathOrientation=RIGHT, path=s1,
        sketchOrientation=RIGHT, profile=s, profileNormal=ON)
    del mdb.models['Model-1'].sketches['__sweep__']
    del mdb.models['Model-1'].sketches['__profile__']

def collagen_top_args(part_name, depth_of_section, collagen_top_thickness, radius_of_curvature, horizontal_section_length, right_thickness, flat_length, protrusion_length, b_points=False):
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior

    p = mdb.models['Model-1'].parts[part_name]
    f, e = p.faces, p.edges
    # t = p.MakeSketchTransform(sketchPlane=f[5], sketchUpEdge=e[11],
    #     sketchPlaneSide=SIDE1, sketchOrientation=RIGHT, origin=(-protrusion_length/2, 0.0,
    #     depth_of_section))
    t = p.MakeSketchTransform(sketchPlane=f[5], sketchUpEdge=e[11],
        sketchPlaneSide=SIDE1, sketchOrientation=RIGHT, origin=(-protrusion_length/4, 0.0,
        depth_of_section))
    # t = p.MakeSketchTransform(sketchPlane=f[5], sketchUpEdge=e[11],
    #     sketchPlaneSide=SIDE1, sketchOrientation=RIGHT, origin=(0.0, 0.0,
    #     depth_of_section))
    # t = p.MakeSketchTransform(sketchPlane=f[5], sketchUpEdge=e[11],
    #     sketchPlaneSide=SIDE1, sketchOrientation=LEFT, origin=(0.0, right_thickness/2,
    #     depth_of_section))
    s = mdb.models['Model-1'].ConstrainedSketch(name='__profile__',
        sheetSize=105.35, gridSpacing=2.63, transform=t)
    g, v, d, c = s.geometry, s.vertices, s.dimensions, s.constraints
    s.setPrimaryObject(option=SUPERIMPOSE)
    p = mdb.models['Model-1'].parts[part_name]
    p.projectReferencesOntoSketch(sketch=s, filter=COPLANAR_EDGES)


    x_axis = s.ConstructionLine(point1=(7.89, 0.0), point2=(13.15, 0.0))
    s.HorizontalConstraint(entity=x_axis, addUndoState=False)
    s.FixedConstraint(entity=x_axis)

    break_point(b_points, "1")

    y_axis_touch = s.ConstructionLine(point1=(right_thickness/2, 0.0), point2=(right_thickness/2, 5.26))
    s.VerticalConstraint(entity=y_axis_touch, addUndoState=False)
    # s.CoincidentConstraint(entity1=y_axis_touch, entity2=v[3])
    s.FixedConstraint(entity=y_axis_touch)

    break_point(b_points, "2")

    s.Line(point1=(right_thickness/2, 13.15), point2=(right_thickness/2, 0.0))
    s.VerticalConstraint(entity=g[8], addUndoState=False)
    s.ParallelConstraint(entity1=g[7], entity2=g[8], addUndoState=False)

    break_point(b_points, "3")

    s.CoincidentConstraint(entity1=v[4], entity2=g[7], addUndoState=False)
    s.CoincidentConstraint(entity1=v[5], entity2=g[6], addUndoState=False)

    break_point(b_points, "4")

    s.Line(point1=(right_thickness/2, 0.0), point2=(right_thickness/2, -10.2053871154785))
    s.VerticalConstraint(entity=g[9], addUndoState=False)
    s.ParallelConstraint(entity1=g[8], entity2=g[9], addUndoState=False)

    break_point(b_points, "5")

    s.CoincidentConstraint(entity1=v[6], entity2=g[7], addUndoState=False)
    s.EqualLengthConstraint(entity1=g[9], entity2=g[8])
    s.VerticalDimension(vertex1=v[6], vertex2=v[4], textPoint=(-15.2956047058105,
        1.6713809967041), value=flat_length)

    break_point(b_points, "6")

    s.ConstructionLine(point1=(18.41, -21.04), point2=(21.04, -21.04))
    s.HorizontalConstraint(entity=g[10], addUndoState=False)
    s.CoincidentConstraint(entity1=v[6], entity2=g[10])

    break_point(b_points, "7")

    s.ConstructionLine(point1=(13.15, 23.67), point2=(18.41, 23.67))
    s.HorizontalConstraint(entity=g[11], addUndoState=False)
    s.CoincidentConstraint(entity1=g[11], entity2=v[4])

    break_point(b_points, "8")


    s.ArcByCenterEnds(center=(right_thickness/2-radius_of_curvature, -flat_length/2), point1=(right_thickness/2, -flat_length/2),
        point2=(3.2875, -flat_length/2-horizontal_section_length), direction=CLOCKWISE)
    break_point(b_points, "8.1")
    s.CoincidentConstraint(entity1=v[8], entity2=g[10], addUndoState=False)
    break_point(b_points, "8.2")

    s.HorizontalDimension(vertex1=v[6], vertex2=v[8], textPoint=(1.63712692260742,
        -14.0401802062988), value=radius_of_curvature)
    break_point(b_points, "8.3")

    s.VerticalDimension(vertex1=v[6], vertex2=v[7], textPoint=(39.6365776062012,
        -47.5099487304688), value=horizontal_section_length)

    break_point(b_points, "9")

    s.ArcByCenterEnds(center=(right_thickness/2-radius_of_curvature-collagen_top_thickness, -flat_length/2), point1=(collagen_top_thickness + right_thickness/2,
        -flat_length/2), point2=(13.8075, -flat_length/2-horizontal_section_length- collagen_top_thickness), direction=CLOCKWISE)
    break_point(b_points, "9.1")
    s.CoincidentConstraint(entity1=v[11], entity2=g[10], addUndoState=False)
    break_point(b_points, "9.2")
    s.CoincidentConstraint(entity1=v[9], entity2=g[10], addUndoState=False)
    s.HorizontalDimension(vertex1=v[9], vertex2=v[6], textPoint=(10.9628486633301,
        -44.4892044067383), value=collagen_top_thickness)
    s.HorizontalDimension(vertex1=v[9], vertex2=v[11], textPoint=(10.9628486633301,
        -44.4892044067383), value=collagen_top_thickness+ radius_of_curvature)

    break_point(b_points, "10")

    con_line = s.Line(point1=(2.63, -42), point2=(13.15, -42))
    break_point(b_points, "10.1")
    s.HorizontalConstraint(entity=g[14], addUndoState=False)
    break_point(b_points, "10.2.1")

    s.CoincidentConstraint(entity1=v[12], entity2=v[7])
    break_point(b_points, "10.2.2")
    s.CoincidentConstraint(entity1=v[13], entity2=v[10])
    break_point(b_points, "10.3")
    # s.ObliqueDimension(vertex1=v[12], vertex2=v[13], textPoint=(4.13760471343994,
    #     -43.1775588989258), value=collagen_top_thickness)
    break_point(b_points, "10.4")
    # s.PerpendicularConstraint(entity1=g[14], entity2=g[13])


    break_point(b_points, "10.5")



    break_point(b_points, "11")

    s.ArcByCenterEnds(center=(right_thickness/2-radius_of_curvature, flat_length/2), point1=(right_thickness/2, flat_length/2),
        point2=(3.2875, flat_length/2+horizontal_section_length), direction=COUNTERCLOCKWISE)
    # s.ArcByCenterEnds(center=(-49.97, 17.5), point1=(7.5, 17.5), point2=(-1.315,
    #     35.505), direction=COUNTERCLOCKWISE)
    break_point(b_points, "11.1")
    s.CoincidentConstraint(entity1=v[15], entity2=g[11], addUndoState=False)
    break_point(b_points, "11.2")
    s.HorizontalDimension(vertex1=v[15], vertex2=v[4], textPoint=(
        -18.0604610443115, 28.4272918701172), value=radius_of_curvature)
    break_point(b_points, "11.3")
    s.VerticalDimension(vertex1=v[4], vertex2=v[14], textPoint=(33.7830619812012,
        26.2830066680908), value=horizontal_section_length)

    break_point(b_points, "12")

    s.ArcByCenterEnds(center=(right_thickness/2-radius_of_curvature-collagen_top_thickness, flat_length/2), point1=(collagen_top_thickness + right_thickness/2,
        flat_length/2), point2=(13.8075, flat_length/2+horizontal_section_length+collagen_top_thickness), direction=COUNTERCLOCKWISE)
    break_point(b_points, "12.1")
    # s.ArcByCenterEnds(center=(-100.19, 17.5), point1=(17.7524999999721, 17.5),
    #     point2=(14.465, 34.8475), direction=COUNTERCLOCKWISE)
    s.CoincidentConstraint(entity1=v[18], entity2=g[11], addUndoState=False)
    break_point(b_points, "12.2")
    s.CoincidentConstraint(entity1=v[16], entity2=g[11], addUndoState=False)

    break_point(b_points, "13")

    s.Line(point1=(3.2875, 42.08), point2=(13.15, 38.135))
    s.CoincidentConstraint(entity1=v[19], entity2=v[14])
    s.CoincidentConstraint(entity1=v[20], entity2=v[17])
    # s.PerpendicularConstraint(entity1=g[17], entity2=g[16])
    s.HorizontalConstraint(entity=g[17], addUndoState=False)

    # s.ObliqueDimension(vertex1=v[19], vertex2=v[20], textPoint=(4.04012870788574,
    #     47.5974197387695), value=collagen_top_thickness)
    s.HorizontalDimension(vertex1=v[16], vertex2=v[4], textPoint=(12.1466503143311,
        46.71435546875), value=collagen_top_thickness)
    s.HorizontalDimension(vertex1=v[16], vertex2=v[18], textPoint=(12.1466503143311,
        46.71435546875), value=collagen_top_thickness+radius_of_curvature)

    break_point(b_points, "14")

    s.Line(point1=(20.3825, 13.8075), point2=(24.985, -11.835))
    s.CoincidentConstraint(entity1=v[21], entity2=v[16])
    s.CoincidentConstraint(entity1=v[22], entity2=v[9])

    break_point(b_points, "15")

    p = mdb.models['Model-1'].parts[part_name]
    f1, e1 = p.faces, p.edges
    p.SolidExtrude(sketchPlane=f1[5], sketchUpEdge=e1[11], sketchPlaneSide=SIDE1,
        sketchOrientation=RIGHT, sketch=s, depth=depth_of_section, flipExtrudeDirection=ON)
    s.unsetPrimaryObject()
    del mdb.models['Model-1'].sketches['__profile__']

def test_make_keratin():
    part_name = 'keratin'
    h_length = 70
    deppth = 10
    radius = 150
    flat_length = 10
    mid_t =10
    edge_t = 2.5
    make_keratin(part_name, h_length, deppth, radius, flat_length, mid_t, edge_t, False)

def make_keratin(part_name, horizontal_length, depth, radius, flat_length, mid_thickness, edge_thickness, b_points):
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior

    s = mdb.models['Model-1'].ConstrainedSketch(name='__profile__',
        sheetSize=200.0)
    g, v, d, c = s.geometry, s.vertices, s.dimensions, s.constraints
    s.setPrimaryObject(option=STANDALONE)
    s.ConstructionLine(point1=(0.0, 0.0), point2=(5.0, 0.0))
    s.HorizontalConstraint(entity=g[2], addUndoState=False)
    s.FixedConstraint(entity=g[2])
    s.Line(point1=(0.0, 20.0), point2=(0.0, 0.0))
    s.VerticalConstraint(entity=g[3], addUndoState=False)
    s.CoincidentConstraint(entity1=v[1], entity2=g[2], addUndoState=False)
    s.Line(point1=(0.0, 0.0), point2=(0.0, -20.0))
    s.VerticalConstraint(entity=g[4], addUndoState=False)
    s.ParallelConstraint(entity1=g[3], entity2=g[4], addUndoState=False)
    s.FixedConstraint(entity=v[1])
    s.EqualLengthConstraint(entity1=g[3], entity2=g[4])
    s.VerticalDimension(vertex1=v[2], vertex2=v[0], textPoint=(-13.3417167663574,
        3.40823745727539), value=flat_length)

    s.Line(point1=(15.0, 20.0), point2=(15.0, 0.0))
    s.VerticalConstraint(entity=g[5], addUndoState=False)
    s.CoincidentConstraint(entity1=v[4], entity2=g[2], addUndoState=False)
    s.Line(point1=(15.0, 0.0), point2=(15.0, -20.0))
    s.VerticalConstraint(entity=g[6], addUndoState=False)
    s.ParallelConstraint(entity1=g[5], entity2=g[6], addUndoState=False)
    s.EqualLengthConstraint(entity1=g[5], entity2=g[6])
    # s.VerticalDimension(vertex1=v[3], vertex2=v[5], textPoint=(31.3922805786133,
    #     -10.0936336517334), value=40.0)
    s.DistanceDimension(entity1=g[6], entity2=g[4], textPoint=(8.240478515625,
        -25.1685371398926), value=mid_thickness)
    session.viewports['Viewport: 1'].view.setValues(nearPlane=160.877,
        farPlane=216.247, width=207.877, height=104.852, cameraPosition=(
        -0.28693, 10.3526, 188.562), cameraTarget=(-0.28693, 10.3526, 0))

    s.ConstructionLine(point1=(0.0, -20.0), point2=(10.0, -20.0))
    s.HorizontalConstraint(entity=g[7], addUndoState=False)
    s.CoincidentConstraint(entity1=v[2], entity2=g[7], addUndoState=False)
    s.CoincidentConstraint(entity1=v[5], entity2=g[7], addUndoState=False)
    s.ConstructionLine(point1=(0.0, 20.0), point2=(10.0, 20.0))
    s.HorizontalConstraint(entity=g[8], addUndoState=False)
    s.CoincidentConstraint(entity1=v[0], entity2=g[8], addUndoState=False)
    s.CoincidentConstraint(entity1=v[3], entity2=g[8], addUndoState=False)

    s.ArcByCenterEnds(center=(-26.25, -flat_length/2), point1=(0.0, -flat_length/2), point2=(-3.75,
        -flat_length), direction=CLOCKWISE)
    s.CoincidentConstraint(entity1=v[7], entity2=g[7], addUndoState=False)
    s.HorizontalDimension(vertex1=v[7], vertex2=v[2], textPoint=(-12.1880397796631,
        -24.4909362792969), value=radius)

    s.VerticalDimension(vertex1=v[2], vertex2=v[6], textPoint=(-58.9383583068848,
        -51.9332504272461), value=horizontal_length)

    s.ArcByCenterEnds(center=(-radius*1.01, -flat_length/2), point1=(mid_thickness, -flat_length/2), point2=(-radius/2,
        -flat_length), direction=CLOCKWISE)
    s.CoincidentConstraint(entity1=v[9], entity2=g[7], addUndoState=False)
    break_point(b_points, "1")

    s.Line(point1=(3.75, -62.5), point2=(16.25, -52.5))
    s.CoincidentConstraint(entity1=v[10], entity2=v[6])
    s.CoincidentConstraint(entity1=v[11], entity2=v[8])
    break_point(b_points, '1.1')
    s.HorizontalDimension(vertex1=v[9], vertex2=v[4], textPoint=(-12.1880397796631,
        -24.4909362792969), value=radius+edge_thickness)
    break_point(b_points, '1.2')

    #*********************************************
    # s.ObliqueDimension(vertex1=v[10], vertex2=v[11], textPoint=(10.5664672851562,
    #     -59.1782608032227), value=edge_thickness)

    s.dragEntity(entity=v[8], points=((-8.04101632804784, -61.5555630594201), (
        -7.5, -61.25), (-5.0, -66.25), (-2.5, -70.0), (-2.5, -71.25)))
    # s.PerpendicularConstraint(entity1=g[11], entity2=g[10])
    s.HorizontalConstraint(entity=g[11], addUndoState=False)

    s.ArcByCenterEnds(center=(-40.0, flat_length/2), point1=(0.0, flat_length/2), point2=(-18.75,
        flat_length), direction=COUNTERCLOCKWISE)
    s.CoincidentConstraint(entity1=v[13], entity2=g[8], addUndoState=False)
    s.ArcByCenterEnds(center=(-radius*1.01, flat_length/2), point1=(mid_thickness, flat_length/2), point2=(-radius/2,
        flat_length), direction=COUNTERCLOCKWISE)
    s.CoincidentConstraint(entity1=v[15], entity2=g[8], addUndoState=False)

    s.VerticalDimension(vertex1=v[0], vertex2=v[12], textPoint=(-22.5618515014648,
        28.9483528137207), value=horizontal_length)
    s.HorizontalDimension(vertex1=v[13], vertex2=v[0], textPoint=(
        -117.643180847168, 93.9401245117188), value=radius)

    s.Line(point1=(-26.25, 57.5), point2=(-15.0, 62.5))
    s.CoincidentConstraint(entity1=v[16], entity2=v[12])
    s.CoincidentConstraint(entity1=v[17], entity2=v[14])
    break_point(b_points, '1.3')
    s.HorizontalDimension(vertex1=v[15], vertex2=v[3], textPoint=(
        -117.643180847168, 93.9401245117188), value=radius+edge_thickness)
    break_point(b_points, '1.4')
    # s.PerpendicularConstraint(entity1=g[13], entity2=g[14])
    s.HorizontalConstraint(entity=g[14], addUndoState=False)

    #*********************************
    # s.ObliqueDimension(vertex1=v[16], vertex2=v[17], textPoint=(-17.6929931640625,
    #     64.6294555664062), value=edge_thickness)
    s.dragEntity(entity=v[13], points=((-48.75, 20.0), (-48.75, 20.0), (-55.0,
        22.5), (-56.25, 22.5), (-65.0, 22.5), (-55.0, 25.0), (-73.75, 25.0), (
        -91.25, 35.0)))

    break_point(b_points, "2")

    p = mdb.models['Model-1'].Part(name=part_name, dimensionality=THREE_D,
        type=DEFORMABLE_BODY)
    p = mdb.models['Model-1'].parts[part_name]
    p.BaseSolidExtrude(sketch=s, depth=depth)
    s.unsetPrimaryObject()
    p = mdb.models['Model-1'].parts[part_name]
    session.viewports['Viewport: 1'].setValues(displayedObject=p)
    del mdb.models['Model-1'].sketches['__profile__']

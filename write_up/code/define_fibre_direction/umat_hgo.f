

SUBROUTINE UMAT(STRESS,STATEV,DDSDDE,SSE,SPD,SCD, &
  RPL,DDSDDT,DRPLDE,DRPLDT, &
  STRAN,DSTRAN,TIME,DTIME,TEMP,DTEMP,PREDEF,DPRED,CMNAME, &
  NDI,NSHR,NTENS,NSTATV,PROPS,NPROPS,COORDS,DROT,PNEWDT, &
  CELENT,DFGRD0,DFGRD1,NOEL,NPT,LAYER,KSPT,KSTEP,KINC)

INCLUDE 'ABA_PARAM.INC'

  CHARACTER*80 CMNAME

  DIMENSION STRESS(NTENS),STATEV(NSTATV), &
  DDSDDE(NTENS,NTENS),DDSDDT(NTENS),DRPLDE(NTENS), &
  STRAN(NTENS),DSTRAN(NTENS),TIME(2),PREDEF(1),DPRED(1), &
  PROPS(NPROPS),COORDS(3),DROT(3,3),DFGRD0(3,3),DFGRD1(3,3), &
  JSTEP(4)

  double precision :: mus(3), alphas(3), k1, k2, K0, a0(3), mu
  doubleprecision :: det
  integer :: N

  N = 3

  ! ! ************ Soft material properties ************
  ! alphas = [5.020e-1, 2.685e1, 2.639e1]
  ! mus = [6.255e6, 8.711e4, 2.652e3]
  ! k1 = 1.956e7
  ! k2 = 3.911

  ! ************ Stiff material properties ************
  alphas = [4.588e-1, 2.679e1, 2.639e1]
  mus = [3.128e7, 4.355e5, 1.326e4]
  k1 = 9.781e7
  k2 = 3.789

  mu = sum(mus)

  ! k1 = 0
  ! k2 = 0.0
  ! a0 = [0., 1., 0.]

  K0 = 5*mu*1e3

  if (KSTEP == 1) then
    call define_fibre_direction(COORDS(2), COORDS(3), a0)
    STATEV(1:3) = a0
    ! STATEV(1) = COORDS(1)
    ! STATEV(2) = COORDS(2)
    ! STATEV(3) = COORDS(3)
    ! STATEV(4) = COORDS(1)
    ! STATEV(5) = COORDS(2)
    ! STATEV(6) = COORDS(3)
    a0 = [0., 1., 0.]
  else
    a0 = STATEV(1:3)
  endif

  ! a0 = [0., 1., 0.]
  call hgo_model(DFGRD1, mus, alphas, N, k1, k2, a0, K0, STRESS, DDSDDE)

  ! print*, "***********************"
  ! print*, "step", KSTEP
  ! print*, "NPT", NPT
  ! print*, "Stress", stress
  ! print*, "J", det(DFGRD1)
  ! print*, "F"
  ! call print_matrix(DFGRD1, 3, 3)
  ! print*,

  RETURN
  END

subroutine define_fibre_direction(y, z, a0)
  doubleprecision :: y, z

  doubleprecision :: pl, pw, ph, gamma, theta, pi, bone_thickness
  doubleprecision :: n(3), p(3)
  doubleprecision :: heaviside

  doubleprecision :: a0(3)
  bone_thickness = 10.0

  ! gamma = 0.25*(y+5)/bone_thickness
  gamma = 0.25*heaviside(-y)

  pi = 3.14159265359
  pl = 15.0
  pw = 2.2
  ph = pl/2.D0
  theta = pi*gamma


  n(2) = pi*ph/(2.D0 * pw)*sin(y*pi/pw - pi)
  ! n(3) = pi*ph/(2.D0 * pw)*sin((z-2)*pi/pw - pi)
  n(1) = -1
  n(3) = 0

  p = n
  n = -n
  p(1) = (p(2)**2 + p(3)**2)**0.5

  p = p /norm2(p)
  n = n /norm2(n)

  a0 = p*cos(theta) + n*sin(theta)
  a0 = a0/norm2(a0)

end

!> @brief Calculates the contribution of the isotropic ogden component of the model to
!! the CTM that Abaqus requires
!!
!! Using the equations:
!!      % Insert formula from write up here
!!
!! @param[in]   lambda_bars  the isochoric principle stretches
!! @param[in]   ns  a matrix with columns containing the principle directions in the current configuration
!! @param[in]   mus an array of material constants \f$\mu\f$
!! @param[in]   alphas an array of material constants \f$\alpha\f$
!! @param[in]   Nps size of the material constant arrays
!! @param[in]   jacobian the determinant of \f$\mathbb{F}\f$
!! @param[in]   sigma the stress contribution of the isochoric ogden strain energy function
!! @param[in]   sigma_p the principle stresses of the isochoric ogden strain energy function
!! @param[in]   \f$i,j,k,l\f$  the indicies of the CTM for which the value must be calculated
!!
!! @return  The value of the entry at index i,j,k,l
subroutine hgo_model(F, mus, alphas, N, k1, k2, a0, K0, sigma_vec, c_mat)
    implicit none

    integer, intent(in) :: N
    doubleprecision, intent(in) :: F(3, 3), mus(N), alphas(N), k1, k2, a0(3), K0

    integer :: i, j, k, l, p, ci, cj, is(6), js(6), info
    doubleprecision :: jacobian, b_bar(3, 3), ns(3, 3), eig_vecs(3, 3), lambda_bars(3), sigma_mat(3, 3), &
    work(16), Id(3, 3), work1, work2, sigma_p(3), a_bar(3), I4_bar, psi4
    integer :: delta
    doubleprecision :: det, ogden_c_bar_ijkl, aniso_C_bar_ijkl

    doubleprecision, intent(out) :: sigma_vec(6), c_mat(6, 6)


    is = [1, 2, 3, 2, 1, 1]
    js = [1, 2, 3, 3, 3, 2]
    sigma_mat = 0
    call identity(Id, 3)

    jacobian = det(F)
    b_bar = matmul(F, transpose(F)) * jacobian ** (-2./3.)

    a_bar = matmul(F, a0) * jacobian ** (-1./3.)
    I4_bar = dot_product(a_bar, a_bar)

    ns = b_bar
    call dsyev('V', 'U', 3, ns, 3, lambda_bars, work, 16, info)
    lambda_bars = lambda_bars ** 0.5

    sigma_p = 0
    do p =1,N
        work1 = -sum(lambda_bars**alphas(p))/3.
        do i=1,3
            sigma_p(i) = sigma_p(i) + (1.0/jacobian) * mus(p) * (lambda_bars(i)**alphas(p) + work1)
        enddo
    enddo

    do i=1,3
        sigma_mat = sigma_mat + sigma_p(i) * spread(ns(:, i),dim=2,ncopies=3)*spread(ns(:, i),dim=1,ncopies=3)
    end do

    do ci = 1,6
        do cj=1,6
              i = is(ci)
              j = js(ci)
              k = is(cj)
              l = js(cj)

              C_mat(ci, cj) = aniso_C_bar_ijkl(jacobian, k1, k2, a_bar, I4_bar, i, j, k, l) + &
                      ogden_c_bar_ijkl(lambda_bars, ns, mus, alphas, N, jacobian, sigma_mat, sigma_p, i, j, k, l)
        enddo
    enddo

    C_mat(1:3, 1:3) = C_mat(1:3, 1:3) + K0 * (2.*jacobian - 1)
    ! print*, 'C_mat dash'
    ! call print_matrix(c_mat, 6, 6)
    !C_mat(:, 4:) = C_mat(:, 4:)/2
    !C_mat(4:, :3) = C_mat(4:, :3)*2.

    sigma_mat = sigma_mat + Id * K0 * (jacobian - 1.)

    psi4 = (I4_bar-1.) * k1 * exp(k2 * (I4_bar-1.)**2.)
    sigma_mat = sigma_mat + (2.0/jacobian) * psi4 &
            * (spread(a_bar,dim=2,ncopies=3)*spread(a_bar,dim=1,ncopies=3) - (1.0/3.0) * I4_bar * Id)
    !  print*, 'sigma_mat'
    !  call print_matrix(sigma_mat, 3, 3)
    !  print*, 'C_mat'
    !  call print_matrix(c_mat, 6, 6)
    !  print*, 'F'
    !  call print_matrix(F, 3, 3)
    !  print*, 'J', jacobian

    call matrix_to_voignt(sigma_mat, sigma_vec)

end subroutine

!> @brief Calculates the contribution of the isotropic ogden component of the model to
!! the CTM that Abaqus requires
!!
!! Using the equations:
!!      % Insert formula from write up here
!!
!! @param[in]   lambda_bars  the isochoric principle stretches
!! @param[in]   ns  a matrix with columns containing the principle directions in the current configuration
!! @param[in]   mus an array of material constants \f$\mu\f$
!! @param[in]   alphas an array of material constants \f$\alpha\f$
!! @param[in]   N size of the material constant arrays
!! @param[in]   jacobian the determinant of \f$\mathbb{F}\f$
!! @param[in]   sigma the stress contribution of the isochoric ogden strain energy function
!! @param[in]   sigma_p the principle stresses of the isochoric ogden strain energy function
!! @param[in]   \f$i,j,k,l\f$  the indicies of the CTM for which the value must be calculated
!!
!! @return  The value of the entry at index i,j,k,l
function ogden_c_bar_ijkl(lambda_bars, ns, mus, alphas, N, jacobian, sigma, sigma_p, i, j, k, l) result(c_val)
    integer, intent(in) :: i, j, k, l, N
    doubleprecision, intent(in) :: lambda_bars(3), ns(3, 3), mus(N), alphas(N), sigma(3, 3), sigma_p(3), jacobian

    doubleprecision :: mult1, mult2, add1
    integer :: a, b, p

    doubleprecision :: det
    integer :: delta

    doubleprecision :: c_val

    c_val = 0

    do a=1,3
        do b=1,3
            mult1 = 0
            do p=1,N
                add1 = sum(lambda_bars**alphas(p))/9.
                mult1 = mult1 + alphas(p) * mus(p) * (lambda_bars(a)**alphas(p) * delta(a, b)  &
                        - lambda_bars(b)**alphas(p)/3. - lambda_bars(a)**alphas(p)/3. + add1)
            end do
            c_val = c_val + mult1 * ns(i, a) * ns(j, a) * ns(k, b) * ns(l, b)
        end do
    end do

    c_val = c_val / jacobian


!    ***************************************************
!    ************* Second bit (Third in notes) *********
!    ***************************************************

    do a=1,3
        do b=1,3
            if (a /= b)then
                if(abs(lambda_bars(a)-lambda_bars(b)) < 1e-8)then
                    mult1 = 0
                    mult2 = 0
                    do p=1,N
                        add1 = sum(lambda_bars**alphas(p))/9.
                        mult1 = mult1 + alphas(p) * mus(p) * (lambda_bars(b)**alphas(p)/3. + add1)
                    end do
                    do p=1,N

                        add1 = sum(lambda_bars**alphas(p))/9.

                        mult2 = mult2 + alphas(p) * mus(p) * (lambda_bars(a)**alphas(p) * delta(a, b) &
                                - lambda_bars(b)**alphas(p)/3. - lambda_bars(a)**alphas(p)/3. + add1)
                    end do
                    mult1 = (mult1 - mult2) / (2.0 * jacobian) - sigma_p(b)
                else
                    mult1 = (sigma_p(a) * lambda_bars(b)**2 - sigma_p(b) * lambda_bars(a)**2) /&
                            (lambda_bars(a)**2 - lambda_bars(b)**2)
                end if
                c_val = c_val + mult1 * (ns(i, a)*ns(j, b)*ns(k, a)*ns(l, b) + ns(i, a)*ns(j, b)*ns(k, b)*ns(l, a))
            end if
        end do
    end do

!    *********************************************
!    ******** Third part (second in notes) *******
!    *********************************************

    do a=1,3
        c_val = c_val - 2. * sigma_p(a) * ns(i, a) * ns(j, a) * ns(k, a) * ns(l, a)
    end do


!    ******************************
!    ****** Convert to Jaumann ****
!    ******************************

        c_val = c_val + jacobian * (sigma(i, k) * delta(j, l) + sigma(i, l) * delta(j, k) + sigma(j, k) * delta(i, l) &
                + delta(i, k) * sigma(j, l))/2.

end function ogden_c_bar_ijkl

!> @brief Calculates the contribution of the anisotropic component of the HGO model to
!! the CTM that Abaqus requires
!!
!! using the equation:
!!      % Insert formula from write up here
!!
!! @param[in]   \f$\mathbb{a}\f$  the isochoric fibre direction in the current configuration (\f$\mathbb{Fa}_0J^{-1/3}\f$)
!! @param[in]   \f$I4\f$  the isochoric 4th invariant (\f$\mathbb{a\cdot a}\f$)
!! @param[in]   \f$k1, k2\f$  the material constants for the HGO model
!! @param[in]   \f$i,j,k,l\f$  the indicies of the CTM for which the value must be calculated
!!
!! @return  The value of the entry at index i,j,k,l
function aniso_C_bar_ijkl(jacobian, k1, k2, a, I4, i, j, k, l) result(c)
    double precision, intent(in) :: a(3), I4, jacobian, k1, k2
    integer, intent(in) :: i, j, k, l
    integer :: delta ! Kronecker delta function
    double precision :: c

    ! Applying the equation
    c = (2.0*k1/jacobian)*exp(k2*(I4-1)**2) * (2.0*(1+2.0*k2*(I4-1)**2) &
    *(a(i)*a(j)-(1.0/3.0)*I4*delta(i,j))*(a(k)*a(l)-(1.0/3.0)*I4*delta(k,l))&
    +(I4-1)*((delta(i,k)*a(j)*a(l) + delta(j,l)*a(i)*a(k) + delta(j,k)*a(i)*a(l) + delta(i,l)*a(j)*a(k))/2 &
    - (2.0/3.0)*(delta(k,l)*a(i)*a(j)+delta(i,j)*a(l)*a(k)) &
    + 2.0/9.0*I4*delta(i,j)*delta(k,l)))
end function aniso_C_bar_ijkl


subroutine voignt_to_matrix(in, out)
  double precision :: in(6), out(3, 3)

  out(1,1) = in(1)
  out(2,2) = in(2)
  out(3,3) = in(3)

  out(2,3) = in(4)
  out(1,3) = in(5)
  out(1,2) = in(6)

  out(3,2) = in(4)
  out(3,1) = in(5)
  out(2,1) = in(6)

end


subroutine  tensor_product_2(mat_A, mat_B, len_a, len_b, out)
  integer :: len_a, len_b
  real:: mat_A(len_a, 1), mat_B(len_b, 1)
  real, dimension(len_a,len_b) :: out
  integer :: i,j

  do i = 1,len_a
    do j = 1,len_b
        out(i,j) = mat_A(i,1) * mat_B(j,1)
    enddo
  enddo
end

subroutine  tensor_product_1(mat_A, mat_B, len_a, len_b, out)
  integer :: len_a, len_b
  double precision:: mat_A(len_a), mat_B(len_b)
  double precision, dimension(len_a,len_b) :: out
  integer :: i,j

  do i = 1,len_a
    do j = 1,len_b
        out(i,j) = mat_A(i) * mat_B(j)
    enddo
  enddo
end


function delta(i, j) result(d)
   integer, intent(in) :: i! input
   integer :: d
   ! integer             :: j ! output
   if (i==j)then
     d = 1
   else
     d = 0
   endif
end function delta

function det(mat) result(d)
  double precision, intent(in) :: mat(3,3)
  double precision :: d

  d = mat(1,1) * (mat(2,2)*mat(3,3) - mat(2, 3) * mat(3,2))
  d = d - mat(1,2) * (mat(2,1)*mat(3,3) - mat(2, 3) * mat(3,1))
  d = d + mat(1,3) * (mat(2,1)*mat(3,2) - mat(2, 2) * mat(3,1))
end function det

function tr(mat) result(d)
  double precision, intent(in) :: mat(3,3)
  double precision :: d

  d = mat(1,1) + mat(2,2) + mat(3,3)
end function tr

function heaviside(x) result(d)
  double precision, intent(in) :: x
  double precision :: d, a

  a= 1.0
  d = 0.5*(sign(a,x)+1.0)
end function heaviside

subroutine identity(in_mat, n)
  integer :: n, i
  double precision :: in_mat(n,n)
  in_mat = 0
  do i = 1,n
      in_mat(i,i) = 1.0
  enddo
end

subroutine matrix_to_voignt(in, out)
  double precision :: in(3,3), out(6)
  out(1) = in(1,1)
  out(2) = in(2,2)
  out(3) = in(3,3)

  out(4) = in(1,2)
  out(5) = in(1,3)
  out(6) = in(2,3)
end

subroutine print_matrix(mat, rows, collumns)
  integer :: rows, collumns
  double precision :: mat(rows, collumns)
  integer :: i, j

  do i =1,rows
      print*,mat(i,:)
  enddo
end

subroutine print_vector(mat, rows)
  integer :: rows
  double precision :: mat(rows)
  integer :: i

  do i =1,rows
      print*,mat(i)
  enddo
end

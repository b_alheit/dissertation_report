from scipy.optimize import curve_fit
import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize as op


data = np.genfromtxt('../data/all_soft.dat', delimiter=',')

# data = data[data[:, 0] > 80]
# data = data[data[:, 0] < 10]
data = data[(data[:, 0] < 10) | (data[:, 0] > 80)]
# data = data[(data[:, 0] < 80) & (data[:, 0] > 70)]
# data = data[(data[:, 0] < 20) & (data[:, 0] > 10)]

x_data = (data[:, 1], data[:, 0])
y_data = data[:, 2]
print("x data")
print(x_data)
print('y_data')
print(y_data)

G = 3.12765722e+07
G = 14565847.848906325
G = 13136419.574349677

k1 = 9.78103723e+07
k2 = 3.78914405e+00
k1/=5

k1 = 10746668.704658937
k2 = 6.5586620611397795
# k1=k2=0



def sigma_hgo_perpendicular(lambda_1, a0, k1, k2, G):
    def R(lam_2):
        return 2* G * (lam_2**2 - (lambda_1 * lam_2) ** (-2)) + 2 * k1*(lam_2**2-1) * np.e**(k2 * (lam_2**2 -1)**2)*lam_2**2
    guess = lambda_1**-0.5
    lambda_2 = op.newton(R, guess)
    sigma_11 = 2 * G * (lambda_1 ** 2 - lambda_2 ** 2) - 2 * k1 * (lambda_2**2 - 1) * np.e ** (k2 * (lambda_2**2 -1) ** 2) * lambda_2**2
    return sigma_11

def sigma_hgo_parallel(lambda_1, a0, k1, k2, G):
    sigma_11 = 2 * G * (lambda_1 ** 2 - lambda_1 ** (-1)) + 2 * k1 * (lambda_1**2 - 1) * np.e ** (k2 * (lambda_1**2 -1) ** 2) * lambda_1**2
    return sigma_11

def find_nominal_stress_given_lambda1(lambda_1, theta, k1, k2, G):
    a0 = np.array([np.cos(np.deg2rad(theta)),
                   np.sin(np.deg2rad(theta)),
                   0])

    if np.linalg.norm(theta - 0) < 1e-10:
        sigma_11 = sigma_hgo_parallel(lambda_1, a0, k1, k2, G)
    elif np.linalg.norm(theta - 90) < 1e-10:
        sigma_11 = sigma_hgo_perpendicular(lambda_1, a0, k1, k2, G)
    else:
        raise ValueError("Theta must either be 0 deg or 90 deg")

    nominal_stress = sigma_11 / lambda_1
    return nominal_stress

def find_stress_from_array(stretch_and_fiber_direction, k1, k2, G):
    # mus = np.array([mu1, mu2, mu3])
    # alphas = np.array([alpha1, alpha2, alpha3])

    lambda_1, theta = stretch_and_fiber_direction[0], stretch_and_fiber_direction[1]

    nominal_stresses = np.array([find_nominal_stress_given_lambda1(lambda_1[i], theta[i], k1, k2, G) for i in
                                 range(np.alen(lambda_1))])

    return nominal_stresses

# p0 = (k1, k2, mu1, mu2, mu3, alpha1, alpha2, alpha3)
# p0 = (G,)
p0 = (k1, k2, G)
# pk_perscribe = (0, 0)
pk_perscribe = ()

def mask_function(x_data, *args):
    nk = int(2 - len(pk_perscribe))

    n_other = int((len(args) - nk))

    ks = pk_perscribe + args[:nk]
    G = np.array(args[nk:nk+n_other])[0]

    print('ks', *ks)
    print('G', G)

    return find_stress_from_array(x_data, *ks, G)


popt = p0
popt, pconv = curve_fit(mask_function,
                        xdata=x_data,
                        ydata=y_data,
                        p0=p0,
                        bounds=([0] * len(p0), [np.inf] * len(p0)))
print('popt')
print(popt)
print('pconv')
print(pconv)

save_array = np.array(popt)
np.savetxt('../data/soft_properties.data', save_array, delimiter=',')

N = mask_function(x_data, *popt)

err = np.mean(100*(N-y_data)/y_data)
print("Error = ", err, " %")

plt.plot(x_data[0], y_data, linewidth=0, marker='s', mfc='none', label='Data')
plt.plot(x_data[0], N, label='model')
plt.grid()
plt.legend()
plt.show()

from scipy.optimize import curve_fit
import numpy as np
import matplotlib.pyplot as plt
from scripts import fitting_functions as ff
import matplotlib
SMALL_SIZE = 8
MEDIUM_SIZE = 10
BIGGER_SIZE = 12

plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=BIGGER_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=BIGGER_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=MEDIUM_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=MEDIUM_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=BIGGER_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title
matplotlib.rcParams['text.usetex'] = True
matplotlib.rcParams['text.latex.preamble'] = [
    r'\usepackage{amsmath}',
    r'\usepackage{amssymb}']

data = np.genfromtxt('../data/all_soft.dat', delimiter=',')
sorted_data = [None] * 2
names = ['0 deg', '90 deg']
colors = ['blue', 'red']
markers = ['s', 'o']
styles = ['-.', '--']
a = 0
for i in range(2):
    if i ==0:
        sorted_data[i] = data[data[:, 0] < 10]
    else:
        sorted_data[i] = data[data[:, 0] > 80]
print(sorted_data)


mu1 = 1.40941639e+05
mu2 = 1.86724174e+04
mu3 = 5.89645086e+03
mus = np.array([mu1, mu2, mu3])

alpha1 = 2.42950714e+01
alpha2 = 2.42816570e+01
alpha3 = 2.42918659e+01
alphas = np.array([alpha1, alpha2, alpha3])

k1 = 1.97966081e+07
k2 = 3.75270408e+00


func = ff.find_stress_from_array
for i in range(2):
    x_data = (sorted_data[i][:, 1], sorted_data[i][:, 0])
    y_data = sorted_data[i][:, 2]

    N = func(x_data, k1, k2, mus, alphas)
    plt.plot(x_data[0], y_data/1e6, linewidth=0, marker=markers[i], mfc='none', label='Data ' + names[i], color=colors[i])
    # plt.plot(x_data[0], N, label='model ' + names[i], color=colors[i], marker='o', mfc='none')
    plt.plot(x_data[0], N/1e6, label='Modified HGO Model ' + names[i], color=colors[i], linestyle=styles[i])

# plt.title("Comparison of model with soft data")
plt.xlabel("$\lambda$")
plt.ylabel("$P$ $(MPa)$")
plt.grid()
plt.legend()
plt.show()

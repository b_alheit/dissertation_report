from scipy.optimize import curve_fit
import numpy as np
import matplotlib.pyplot as plt
from scripts import fitting_functions as ff


data = np.genfromtxt('../data/all_stiff.dat', delimiter=',')

# data = data[data[:, 0] > 80]
# data = data[data[:, 0] < 10]
data = data[(data[:, 0] < 10) | (data[:, 0] > 80)]
# data = data[(data[:, 0] < 80) & (data[:, 0] > 70)]
# data = data[(data[:, 0] < 20) & (data[:, 0] > 10)]

x_data = (data[:, 1], data[:, 0])
y_data = data[:, 2]
print("x data")
print(x_data)
print('y_data')
print(y_data)

mu1 = 3.12765722e+07
mu2 = 4.35548303e+05
mu3 = 1.32582581e+04

alpha1 = 4.58761314e-01
alpha2 = 2.67884507e+01
alpha3 = 2.63919789e+01


k1 = 9.78103723e+07
k2 = 3.78914405e+00

# mu1 /= 5
# mu2 /= 5
# mu3 /= 5
# k1 /= 5

p0 = (k1, k2, mu1, mu2, mu3, alpha1, alpha2, alpha3)
# pk_perscribe = (0, 0)
pk_perscribe = ()
# p0 = (mu1, alpha1)
# p0 = (mu1, mu2, alpha1, alpha2)
# p0 = (mu1, mu2, mu3, alpha1, alpha2, alpha3)
# p0 = (k1, k2, mu1, alpha1)
# p0 = (mu1, mu2, mu3, alpha1, alpha2, alpha3)
# p0 = (k1, k2, mu1, mu2, mu3, alpha1, alpha2, alpha3)
def mask_function(x_data, *args):
    nk = int(2 - len(pk_perscribe))

    n_other = int((len(args) - nk)/2)

    ks = pk_perscribe + args[:nk]
    mus = np.array(args[nk:nk+n_other])
    alphas = np.array(args[nk+n_other:])

    print('ks', *ks)
    print('mus', mus)
    print('alphas', alphas)

    return ff.find_stress_from_array(x_data, *ks, mus, alphas)

# p0 = (k1, k2, mu1, mu2, mu3, alpha1, alpha2, alpha3)
# p0 = (mu1, alpha1)
# p0 = (k1, k2, mu1, alpha1)
# p0 = (mu2, alpha2)
# p0 = (mu1, mu2, alpha1, alpha2)
# p0 = [1.99999720e+05, 3.99993293e+03, 9.99990116e+00, 9.99998031e+01,
#  2.00000859e+02, 8.00002345e+04, 8.00001065e+04, 6.00010457e+03]

fit_func = ff.find_stress_from_array
popt = p0
# popt, pconv = curve_fit(fit_func, xdata=x_data, ydata=y_data, p0=p0, bounds=([0, -np.inf], [np.inf, np.inf]))
# popt, pconv = curve_fit(fit_func, xdata=x_data, ydata=y_data, p0=p0, bounds=([0, 0], [np.inf, np.inf]))
popt, pconv = curve_fit(mask_function,
                        xdata=x_data,
                        ydata=y_data,
                        p0=p0,
                        bounds=([0] * len(p0), [np.inf] * len(p0)))
print('popt')
print(popt)
print('pconv')
print(pconv)

save_array = np.array(popt)
np.savetxt('../data/stiff_properties.data', save_array, delimiter=',')

N = mask_function(x_data, *popt)

err = np.mean(100*(N-y_data)/y_data)
print("Error = ", err, " %")

plt.plot(x_data[0], y_data, linewidth=0, marker='s', mfc='none', label='Data')
plt.plot(x_data[0], N, label='model')
plt.grid()
plt.legend()
plt.show()

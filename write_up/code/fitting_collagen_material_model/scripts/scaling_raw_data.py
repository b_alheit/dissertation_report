import numpy as np
import matplotlib.pyplot as plt
import pprint
import matplotlib

SMALL_SIZE = 8
MEDIUM_SIZE = 10
BIGGER_SIZE = 12

plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=BIGGER_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=BIGGER_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=MEDIUM_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=MEDIUM_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=BIGGER_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title

matplotlib.rcParams['text.usetex'] = True
matplotlib.rcParams['text.latex.preamble'] = [
    r'\usepackage{amsmath}',
    r'\usepackage{amssymb}']


data = np.genfromtxt('../data/raw_data.csv', delimiter=',')
names = ['0$^0$', '15$^0$', '30$^0$', '45$^0$', '60$^0$', '75$^0$', '90$^0$']
markers = ['s', 'd', 'o', '^', 'v', 'h', '*']
fit_grad = 200

strain_45_deg = data[:, 6]
stress_45_deg = data[:, 7]

pprint.pprint(data)
print()
pprint.pprint(data[:, 0::2])

front = (strain_45_deg < 0.1) & (strain_45_deg > 0.02)
avg_grad_front = np.mean(stress_45_deg[front] / strain_45_deg[front])

back = strain_45_deg > 0.2
avg_grad_back = np.mean(stress_45_deg[back] / strain_45_deg[back])

print("Front grad: ", avg_grad_front, " MPa")
print("Back grad: ", avg_grad_back, " MPa")

back_sf = fit_grad/avg_grad_back
front_sf = fit_grad/avg_grad_front

# stress_front = data[:, 1::2] * front_sf ** 0.5
# strain_front = data[:, 0::2] * front_sf ** -0.5
# stress_back = data[:, 1::2] * back_sf ** 0.5
# strain_back = data[:, 0::2] * back_sf ** -0.5

stress_front = data[:, 1::2] * front_sf
strain_front = data[:, 0::2]
stress_back = data[:, 1::2] * back_sf
strain_back = data[:, 0::2]

strain_45_deg_front = strain_front[:, 3]
stress_45_deg_front = stress_front[:, 3]

avg_grad_front_test = np.mean(stress_45_deg_front[front] / strain_45_deg_front[front])

print("Average grad front test:", avg_grad_front_test)

angle_array = np.zeros(np.shape(stress_front))
print(np.shape(stress_front))
angles = [0, 15, 30, 45, 60, 75, 90]
for i in range(len(angles)):
    angle_array[:, i] = angles[i]

all_stress = []
all_stretch = []
all_angle = []

for i in range(len(names)):
    logic = np.logical_not(np.isnan(stress_front[:, i]))

    all_stress = all_stress + (stress_front[:, i][logic] * 1e6).tolist()
    all_stretch = all_stretch + (strain_front[:, i][logic] + 1).tolist()
    all_angle = all_angle + (angle_array[:, i][logic]).tolist()

all_stiff = np.array([all_angle,
                      all_stretch,
                      all_stress]).T

# np.savetxt('../data/all_stiff.dat', all_stiff, delimiter=',')

all_stress = []
all_stretch = []
all_angle = []

for i in range(len(names)):
    logic = np.logical_not(np.isnan(stress_back[:, i]))

    all_stress = all_stress + (stress_back[:, i][logic] * 1e6).tolist()
    all_stretch = all_stretch + (strain_back[:, i][logic] + 1).tolist()
    all_angle = all_angle + (angle_array[:, i][logic]).tolist()

all_soft = np.array([all_angle,
                     all_stretch,
                     all_stress]).T

# np.savetxt('../data/all_soft.dat', all_soft, delimiter=',')


# np.savetxt('stiff_stress_nom.dat', stress_front * 1e6, delimiter=',')
# np.savetxt('stiff_F11.dat', strain_front + 1, delimiter=',')
# np.savetxt('soft_stress_nom.dat', stress_back * 1e6, delimiter=',')
# np.savetxt('soft_F11.dat', strain_back + 1, delimiter=',')
# np.savetxt('angels.dat', angle_array, delimiter=',')

# for i in range(len(names)):
    # plt.plot(strain_front[:, i] + 1, stress_front[:, i] * 1e6, marker=markers[i], mfc="none", linewidth=0, label=names[i])
    # plt.plot(strain_back[:, i], stress_back[:, i], marker=markers[i], mfc="none", linewidth=0, label=names[i])

strain_straight = np.array([0]+strain_back[:, -1].tolist())
stress_straight = np.array([0]+(strain_back[:, -1]*200).tolist())
stress_straight = stress_straight[np.logical_not(np.isnan(stress_straight))]
strain_straight = strain_straight[np.logical_not(np.isnan(strain_straight))]
# plt.plot([0]+strain_back[:, -1].tolist(),[0] + (strain_back[:, -1] * 200).tolist(), color='black', label='0.2 GPa line')
plt.plot(strain_straight+1, stress_straight, color='black', label='0.2 GPa line')
plt.plot(strain_front[:, 3]+1, stress_front[:, 3], marker='s', mfc="none", linewidth=0, label="Stiff scaled $45^\circ$", color="blue")
plt.plot(strain_back[:, 3]+1, stress_back[:, 3], marker='o', mfc="none", linewidth=0, label="Soft scaled $45^\circ$", color="red")


# plt.plot(strain_45_deg, stress_45_deg, marker='o', linewidth=0)
# plt.plot(strain_45_deg, strain_45_deg*avg_grad_front)
# plt.plot(strain_45_deg, strain_45_deg*avg_grad_back)
# plt.title('Comparison of soft and stiff scaled $45^\circ$ curves with 0.2 GPa line')
plt.legend()
plt.grid()
plt.ylabel("$P$ $(MPa)$")
plt.xlabel("$\lambda$")
plt.show()
import numpy as np
from scripts import original_material_model as om


def sigma_bar_aniso_shear(F, a0, k3, k4):
    J = np.linalg.det(F)
    a_bar = np.matmul(F, a0) * J ** (-1/3)
    b_bar = np.matmul(F, F.T) * J ** (-2/3)
    C_bar = np.matmul(F.T, F) * J ** (-2/3)
    I4_bar = np.matmul(a0.T, np.matmul(C_bar, a0))
    I5_bar = np.matmul(a0.T, np.matmul(C_bar, np.matmul(C_bar, a0)))

    tens1 = np.outer(a_bar, a_bar) - 1/3 * I4_bar * np.eye(3, 3)
    tens2 = np.outer(a_bar, np.matmul(b_bar, a_bar)) + np.outer(np.matmul(b_bar, a_bar), a_bar) - 2/3 * I5_bar * np.eye(3, 3)

    sigma_bar = (2.0/J) * (k3*np.e**(k4*(I5_bar - I4_bar**2)) * tens2 - 2*k3*I4_bar*np.e**(k4*(I5_bar - I4_bar**2))
                           * tens1)

    return sigma_bar


def sigma(F, a0, k1, k2, k3, k4, K, mus, alphas):
    return om.sigma(F, a0, k1, k2, K, mus, alphas) + sigma_bar_aniso_shear(F, a0, k3, k4)


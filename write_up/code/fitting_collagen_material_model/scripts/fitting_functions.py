import numpy as np
import scipy.optimize as op


def sigma_hgo_parallel(lambda_1, a0, k1, k2, mus, alphas):

    # F = np.zeros([3, 3])
    # F[0, 0] = lambda_1
    # F[1, 1] = lambda_1**-0.5
    # F[2, 2] = lambda_1**-0.5

    # ##################
    # # way 1
    # ##################
    # J = np.linalg.det(F)
    # iso = sigma_bar_iso(F, mus, alphas)
    # iso -= iso[2, 2] * np.eye(3, 3)
    # aniso = sigma_bar_aniso_old(F, a0, k1, k2)
    # aniso -= aniso[2, 2] * np.eye(3, 3)
    # sigma_mat = iso + aniso
    # sigma_mat -= sigma_mat[2, 2] * np.eye(3, 3)
    # sigma_11 = sigma_mat[0, 0]

    ###################
    # Way 2
    ###################

    sigma_11 = np.sum(mus * (lambda_1 ** alphas - lambda_1 ** (-alphas/2.))) + 2 * k1 * (lambda_1**2 - 1) * np.e ** (k2 * (lambda_1**2 -1) ** 2) * lambda_1**2
    return sigma_11


def sigma_hgo_perpendicular(lambda_1, a0, k1, k2, mus, alphas):
    def R(lam_2):
        return np.sum(mus * (lam_2**alphas - (lambda_1 * lam_2) ** (-alphas))) + 2 * k1*(lam_2**2-1) * np.e**(k2 * (lam_2**2 -1)**2)*lam_2**2
    guess = lambda_1**-0.5
    lambda_2 = op.newton(R, guess)

    # F = np.zeros([3, 3])
    # F[0, 0] = lambda_1
    # F[1, 1] = lambda_2
    # F[2, 2] = 1/(lambda_1 * lambda_2)

    ##################
    ## way 1
    ##################
    # J = np.linalg.det(F)
    # iso = sigma_bar_iso(F, mus, alphas)
    # # iso -= iso[2, 2] * np.eye(3, 3)
    # aniso = sigma_bar_aniso_old(F, a0, k1, k2)
    # # aniso -= aniso[2, 2] * np.eye(3, 3)
    # sigma_mat = iso + aniso
    # sigma_mat -= sigma_mat[2, 2] * np.eye(3, 3)
    # sigma_11 = sigma_mat[0,0]

    ###################
    # Way 2
    ###################
    sigma_11 = np.sum(mus * (lambda_1 ** alphas - lambda_2 ** alphas)) - 2 * k1 * (lambda_2**2 - 1) * np.e ** (k2 * (lambda_2**2 -1) ** 2) * lambda_2**2
    return sigma_11


def find_nominal_stress_given_lambda1(lambda_1, theta, k1, k2, mus, alphas):
    a0 = np.array([np.cos(np.deg2rad(theta)),
                   np.sin(np.deg2rad(theta)),
                   0])

    if np.linalg.norm(theta - 0) < 1e-10:
        sigma_11 = sigma_hgo_parallel(lambda_1, a0, k1, k2, mus, alphas)
    elif np.linalg.norm(theta - 90) < 1e-10:
        sigma_11 = sigma_hgo_perpendicular(lambda_1, a0, k1, k2, mus, alphas)
    else:
        raise ValueError("Theta must either be 0 deg or 90 deg")

    nominal_stress = sigma_11 / lambda_1
    return nominal_stress


def find_stress_from_array(stretch_and_fiber_direction, k1, k2, mus, alphas):
    # mus = np.array([mu1, mu2, mu3])
    # alphas = np.array([alpha1, alpha2, alpha3])

    lambda_1, theta = stretch_and_fiber_direction[0], stretch_and_fiber_direction[1]

    nominal_stresses = np.array([find_nominal_stress_given_lambda1(lambda_1[i], theta[i], k1, k2, mus, alphas) for i in
                                 range(np.alen(lambda_1))])

    return nominal_stresses

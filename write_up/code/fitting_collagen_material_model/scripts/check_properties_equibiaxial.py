import numpy as np
import matplotlib.pyplot as plt
from scripts import model


mu1 = 3.12765722e+07
mu2 = 4.35548303e+05
mu3 = 1.32582581e+04
mus = np.array([mu1, mu2, mu3])

alpha1 = 4.58761314e-01
alpha2 = 2.67884507e+01
alpha3 = 2.63919789e+01
alphas = np.array([alpha1, alpha2, alpha3])

k1 = 9.78103723e+07
k2 = 3.78914405e+00
# k1 = k2 = 0
a0 = np.array([1, 0, 0])

K = mus.sum() * 3.5e3

F = np.eye(3, 3)

n_points = 100

stretch = np.linspace(0.9, 1.2, n_points)
sigma_11 = np.zeros(n_points)
sigma_22 = np.zeros(n_points)
sig_diff = np.zeros(n_points)

for i in range(n_points):
    F[0, 0] = F[1, 1] = stretch[i]
    # F[1, 1] = 1.1
    # F[1, 1] = F[0, 0] + (F[0, 0] - 1)*1.2
    F[2, 2] = 1 / (F[0, 0] * F[1, 1])
    sig = model.sigma_hgo(F, a0, k1, k2, K, mus, alphas)
    sig -= np.eye(3, 3) * sig[2, 2]
    sigma_11[i] = sig[0, 0]
    sigma_22[i] = sig[1, 1]

sig_diff = sigma_11 - sigma_22

plt.plot(stretch, sigma_11, label=r'$\sigma_{11}$')
plt.plot(stretch, sigma_22, label=r'$\sigma_{22}$')
# plt.plot(stretch, sig_diff, label=r'$\tilde{\sigma}$')
plt.xlabel(r'$\lambda$')
plt.ylabel(r"$\sigma$")
plt.legend()
plt.grid()
plt.show()

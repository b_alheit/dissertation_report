import scipy.optimize as op
import numpy as np
import matplotlib.pyplot as plt
import matplotlib

SMALL_SIZE = 8
MEDIUM_SIZE = 10
BIGGER_SIZE = 12

plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=BIGGER_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=BIGGER_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=MEDIUM_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=MEDIUM_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=BIGGER_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title

matplotlib.rcParams['text.usetex'] = True
matplotlib.rcParams['text.latex.preamble'] = [
    r'\usepackage{amsmath}',
    r'\usepackage{amssymb}']

data = np.genfromtxt('../data/all_soft.dat', delimiter=',')
sorted_data = [None] * 2
names = ['0 deg', '90 deg']
colors = ['blue', 'red']
markers = ['s', 'o']
styles = ['-.', '--']
a = 0
for i in range(2):
    if i ==0:
        sorted_data[i] = data[data[:, 0] < 10]
    else:
        sorted_data[i] = data[data[:, 0] > 80]
print(sorted_data)


# mu1 = 1.40941639e+05
# mu2 = 1.86724174e+04
# mu3 = 5.89645086e+03
# mus = np.array([mu1, mu2, mu3])
#
# alpha1 = 2.42950714e+01
# alpha2 = 2.42816570e+01
# alpha3 = 2.42918659e+01
# alphas = np.array([alpha1, alpha2, alpha3])
#
# k1 = 1.97966081e+07
# k2 = 3.75270408e+00

k1 = 10746668.704658937
k2 = 6.5586620611397795


G = 13136419.574349677


def sigma_hgo_perpendicular(lambda_1, a0, k1, k2, G):
    def R(lam_2):
        return 2* G * (lam_2**2 - (lambda_1 * lam_2) ** (-2)) + 2 * k1*(lam_2**2-1) * np.e**(k2 * (lam_2**2 -1)**2)*lam_2**2
    guess = lambda_1**-0.5
    lambda_2 = op.newton(R, guess)
    sigma_11 = 2 * G * (lambda_1 ** 2 - lambda_2 ** 2) - 2 * k1 * (lambda_2**2 - 1) * np.e ** (k2 * (lambda_2**2 -1) ** 2) * lambda_2**2
    return sigma_11

def sigma_hgo_parallel(lambda_1, a0, k1, k2, G):
    sigma_11 = 2 * G * (lambda_1 ** 2 - lambda_1 ** (-1)) + 2 * k1 * (lambda_1**2 - 1) * np.e ** (k2 * (lambda_1**2 -1) ** 2) * lambda_1**2
    return sigma_11

def find_nominal_stress_given_lambda1(lambda_1, theta, k1, k2, G):
    a0 = np.array([np.cos(np.deg2rad(theta)),
                   np.sin(np.deg2rad(theta)),
                   0])

    if np.linalg.norm(theta - 0) < 1e-10:
        sigma_11 = sigma_hgo_parallel(lambda_1, a0, k1, k2, G)
    elif np.linalg.norm(theta - 90) < 1e-10:
        sigma_11 = sigma_hgo_perpendicular(lambda_1, a0, k1, k2, G)
    else:
        raise ValueError("Theta must either be 0 deg or 90 deg")

    nominal_stress = sigma_11 / lambda_1
    return nominal_stress

def find_stress_from_array(stretch_and_fiber_direction, k1, k2, G):
    # mus = np.array([mu1, mu2, mu3])
    # alphas = np.array([alpha1, alpha2, alpha3])

    lambda_1, theta = stretch_and_fiber_direction[0], stretch_and_fiber_direction[1]

    nominal_stresses = np.array([find_nominal_stress_given_lambda1(lambda_1[i], theta[i], k1, k2, G) for i in
                                 range(np.alen(lambda_1))])

    return nominal_stresses


for i in range(2):
    x_data = (sorted_data[i][:, 1], sorted_data[i][:, 0])
    y_data = sorted_data[i][:, 2]


    N = find_stress_from_array(x_data, k1, k2, G)
    plt.plot(x_data[0], y_data/1e6, linewidth=0, marker=markers[i], mfc='none', label='Data ' + names[i], color=colors[i])
    # plt.plot(x_data[0], N, label='model ' + names[i], color=colors[i], marker='o', mfc='none')
    plt.plot(x_data[0], N/1e6, label='Original HGO Model ' + names[i], color=colors[i], linestyle=styles[i])

# plt.title("Comparison of Optimised Original HGO Model With Soft Data")
plt.xlabel("$\lambda$")
plt.ylabel("$P$ $(MPa)$")
plt.grid()
plt.legend()
plt.show()

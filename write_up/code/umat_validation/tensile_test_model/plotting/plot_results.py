import numpy as np
import scipy.optimize as op
import matplotlib.pyplot as plt
import matplotlib
matplotlib.rc('text', usetex=True)
matplotlib.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
SMALL_SIZE = 8
MEDIUM_SIZE = 10
BIGGER_SIZE = 12

plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=BIGGER_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=BIGGER_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=MEDIUM_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=MEDIUM_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=MEDIUM_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title


n_points = 1000

alphas = np.array([5.020e-1, 2.685e1, 2.639e1])
mus = np.array([[6.255e6, 8.711e4, 2.652e3]])
k1 = 1.956e7
k2 = 3.911

def par_sigma(s):
    return np.sum(mus * (s**alphas - s **(-alphas/2))) + 2*k1*(s**2 - 1)*np.e**(k2*(s**2 - 1)**2) * s**2

def per_sigma(s1):
    R = lambda s2: np.sum(mus * (s2**alphas - (s1*s2) **(-alphas))) + 2*k1*(s2**2-1)*np.e**(k2*(s2**2-1)**2)*s2**2
    guess = s1**(-1./2.)
    s2 = op.newton(R, guess)
    return np.sum(mus * (s1**alphas - (s1*s2) ** (-alphas))), s2


s11_par = np.loadtxt("../results/s11_par.dat")/1e6
s22_par = np.loadtxt("../results/s_other_par.dat")/1e6
u1_par = np.loadtxt("../results/u1_par.dat")
lambda1_par = (u1_par + 1) / 1
lambdas_par = np.linspace(lambda1_par[0], lambda1_par[-1], n_points)
sigma11_par = np.array([par_sigma(s) for s in lambdas_par]) / 1e6

s11_per = np.loadtxt("../results/s11_per.dat")/1e6
s22_per = np.loadtxt("../results/s_other_per.dat")/1e6
u1_per = np.loadtxt("../results/u1_per.dat")
lambda1_per = (u1_per + 1) / 1
lambdas_per = np.linspace(lambda1_per[0], lambda1_per[-1], n_points)
sigma11_per = np.array([per_sigma(s)[0] for s in lambdas_per]) / 1e6

lambda2_per = np.array([per_sigma(s)[1] for s in lambdas_per])
u2_per = np.loadtxt("../results/u2_per.dat")
aba_lambda2_per = (u2_per + 1) / 1

plt.figure(1)
plt.plot(lambdas_par, sigma11_par, linestyle='--', color='red', label="$||$ Analytical $ \sigma_{11}$")
plt.plot(lambda1_par, s11_par, color='orange', marker='s', mfc='none', linewidth=0, label="$||$ Abaqus $\sigma_{11}$")
plt.plot(lambda1_par, s22_par, color='#c8c800ff', marker='h', mfc='none', linewidth=0, label=r"$||$ Abaqus $||\boldsymbol{\sigma}_{other}||$")

plt.plot(lambdas_per, sigma11_per, color='blue', linestyle='-.', label="$\perp$ Analytical $ \sigma_{11}$")
plt.plot(lambda1_per, s11_per, color='#8000ffff', marker='o', mfc='none', linewidth=0, label="$\perp$ Abaqus $\sigma_{11}$")
plt.plot(lambda1_per, s22_per, color='#cc00ff', marker='*', mfc='none', linewidth=0, label=r"$\perp$ Abaqus $||\boldsymbol{\sigma}_{other}||$")

plt.legend()
plt.ylabel("$\sigma$ ($MPa$)")
plt.xlabel("$\lambda$")
plt.grid()
plt.title("Testing UMAT by Comparing Uniaxial Loading Results With Analytical Solution")
plt.tight_layout()

plt.figure(2)
plt.plot(lambdas_per, lambda2_per, label="Analytical")
plt.plot(lambda1_per, aba_lambda2_per, marker='o', mfc='none', linewidth=0, label='Abaqus')

plt.legend()
plt.grid()
plt.xlabel("$\lambda_1$")
plt.ylabel("$\lambda_2$")
# plt.title("Comparison of Second Principle")

plt.tight_layout()
plt.show()
# -*- coding: mbcs -*-
# Do not delete the following import lines
from abaqus import *
from abaqusConstants import *
import __main__

def Macro1():
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior
    session.viewports['Viewport: 1'].assemblyDisplay.setValues(loads=ON, bcs=ON,
        predefinedFields=ON, connectors=ON)
    mdb.models['Model-1'].boundaryConditions['displace_to_start'].setValues(
        u1=0.12)


def Macro2():
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior
    # session.viewports['Viewport: 1'].assemblyDisplay.setValues(loads=OFF, bcs=OFF,
    #     predefinedFields=OFF, connectors=OFF)
    # mdb.jobs['Job-1'].writeInput(consistencyChecking=OFF)
    # mdb.jobs['Job-1'].submit(consistencyChecking=OFF)
    # session.mdbData.summary()
    o3 = session.openOdb(
        name='/home/cerecam/Benjamin_Alheit/Projects/masters-disertation/dissertation_report/write_up/code/umat_validation/tensile_test_model/Job-1.odb')
    # session.viewports['Viewport: 1'].setValues(displayedObject=o3)
    lastFrame = o3.steps['compress'].frames[-1]
    # For each field output value in the last frame,
    # print the name, description, and type members.
    for f in lastFrame.fieldOutputs.values():
        print(f.name, ':', f.description)
        print('Type: ', f.type)
        # For each location value, print the position.
        for loc in f.locations:
            print('Position:',loc.position)
        print()
    displacement=lastFrame.fieldOutputs['S']
    fieldValues=displacement.values
    print("Field Values")
    print(fieldValues)
    for v in fieldValues:
        print(v)
        print()
        print(v.data)

def run_various_stretches():
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior
    import numpy as np

    n_points = 20
    us = np.linspace(-0.27, 0.27, n_points)

    # print(us)
    for i in range(n_points):
        u1 = us[i]
        job_name = 'u1' + str(u1).replace('.', '')[:4]
        # print(job_name)
        mdb.models['Model-1'].boundaryConditions['displace_to_start'].setValues(
            u1=u1)
        mdb.Job(name=job_name, model='Model-1', description='', type=ANALYSIS,
            atTime=None, waitMinutes=0, waitHours=0, queue=None, memory=90,
            memoryUnits=PERCENTAGE, getMemoryFromAnalysis=True,
            explicitPrecision=SINGLE, nodalOutputPrecision=SINGLE, echoPrint=OFF,
            modelPrint=OFF, contactPrint=OFF, historyPrint=OFF,
            userSubroutine='umat_hgo.f', scratch='', resultsFormat=ODB,
            multiprocessingMode=DEFAULT, numCpus=1, numGPUs=0)
        mdb.jobs[job_name].writeInput(consistencyChecking=OFF)
        mdb.jobs[job_name].submit(consistencyChecking=OFF)
        session.mdbData.summary()

def convert_data_to_txt():
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior
    import numpy as np
    # session.viewports['Viewport: 1'].assemblyDisplay.setValues(loads=OFF, bcs=OFF,
    #     predefinedFields=OFF, connectors=OFF)
    # mdb.jobs['Job-1'].writeInput(consistencyChecking=OFF)
    # mdb.jobs['Job-1'].submit(consistencyChecking=OFF)
    # session.mdbData.summary()
    n_points = 20
    us = np.linspace(-0.27, 0.27, n_points)
    s11 = np.zeros(n_points)
    s22 = np.zeros(n_points)
    u2 = np.zeros(n_points)

    # print(us)
    for i in range(n_points):
        u1 = us[i]
        job_name = 'u1' + str(u1).replace('.', '')[:4]

        o3 = session.openOdb(
            name='/home/cerecam/Benjamin_Alheit/Projects/masters-disertation/dissertation_report/write_up/code/umat_validation/tensile_test_model/'+job_name+'.odb')
        lastFrame = o3.steps['compress'].frames[-1]

        stress_field = lastFrame.fieldOutputs['S']
        stress_node_1 = stress_field.values[0].data
        s11[i] = stress_node_1[0]
        s22[i] = np.linalg.norm(stress_node_1[1:])

        displacement_field = lastFrame.fieldOutputs['U']
        u2s = np.array([displacement_field.values[j].data[1] for j in range(np.alen(displacement_field.values))])
        u2[i] = u2s[np.argmax(np.abs(u2s))]

    np.savetxt("./results/s11_per.dat", s11)
    np.savetxt("./results/s_other_per.dat", s22)
    np.savetxt("./results/u1_per.dat", us)
    np.savetxt("./results/u2_per.dat", u2)

def Macro3():
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior
    a = mdb.models['Model-1'].rootAssembly
    session.viewports['Viewport: 1'].setValues(displayedObject=a)
    session.mdbData.summary()
    o3 = session.openOdb(
        name='/home/cerecam/Benjamin_Alheit/Projects/masters-disertation/dissertation_report/write_up/code/umat_validation/tensile_test_model/Job-1.odb')
    session.viewports['Viewport: 1'].setValues(displayedObject=o3)
    session.viewports['Viewport: 1'].odbDisplay.setSymbolVariable(
        variableLabel='LE', outputPosition=INTEGRATION_POINT,
        tensorQuantity=ALL_DIRECT_COMPONENTS, )

import numpy as np
import original_material_model as om
import matplotlib.pyplot as plt
import matplotlib
matplotlib.rc('text', usetex=True)
matplotlib.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
SMALL_SIZE = 8
MEDIUM_SIZE = 10
BIGGER_SIZE = 12

plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=BIGGER_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=BIGGER_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=MEDIUM_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=MEDIUM_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=MEDIUM_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title


n_points = 1000

alphas = np.array([5.020e-1, 2.685e1, 2.639e1])
mus = np.array([[6.255e6, 8.711e4, 2.652e3]])
k1 = 1.956e7
k2 = 3.911

K0 = 5*np.sum(mus)*1e2

def sigma_shear(c, a0):
    F = np.eye(3, 3)
    F[1, 0] = c
    # F = np.array([[1.0000000000000000, -5.2757736147542147E-033, -7.7453235286685941E-032],
    #               [0.50059943819125885, 1.0006124445331495, -1.3005868211235422E-005],
    #               [-4.9841586249000315E-032,   7.6692012189792089E-032 ,  1.0000000000000000]])
    b = np.matmul(F, F.T)
    lambdas, ns = np.linalg.eig(b)
    lambdas = lambdas ** 0.5

    lamba_sums = np.outer(lambdas, np.ones(np.alen(mus)))
    lamba_sums = -1/3 * np.sum(lamba_sums**alphas, axis=0)

    sigma = np.zeros([3, 3])

    for i in range(3):
        coeff = np.sum(mus * (lambdas[i] ** alphas + lamba_sums))
        sigma += coeff * np.outer(ns[:, i], ns[:, i])

    a = np.matmul(F, a0)
    I4 = np.dot(a, a)

    sigma += 2*(I4-1)*k1*np.e**(k2*(I4-1)**2)*(np.outer(a, a) - (1./3.)*I4 * np.eye(3, 3))
    sigma = om.sigma(F, a0, k1, k2, K0, mus, alphas)
    # sigma -= np.eye(3, 3) * sigma[2, 2]
    return sigma[0, 0], sigma[1, 1], sigma[2, 2], sigma[0, 1]

test = sigma_shear(0, np.array([1., 0., 0.]))
cs = np.linspace(0, 0.5, n_points)

a0_par = np.array([1., 0., 0.])
sigma_par = np.array([sigma_shear(c, a0_par) for c in cs])/1e6
s11_par = np.loadtxt('../results/s11_par.dat')/1e6
s22_par = np.loadtxt('../results/s22_par.dat')/1e6
s33_par = np.loadtxt('../results/s33_par.dat')/1e6
s12_par = np.loadtxt('../results/s12_par.dat')/1e6
so_par = np.loadtxt('../results/so_par.dat')/1e6
c_par = np.loadtxt('../results/u2_par.dat')


a0_per = np.array([0., 1., 0.])
sigma_per = np.array([sigma_shear(c, a0_per) for c in cs])/1e6
s11_per = np.loadtxt('../results/s11_per.dat')/1e6
s22_per = np.loadtxt('../results/s22_per.dat')/1e6
s33_per = np.loadtxt('../results/s33_per.dat')/1e6
s12_per = np.loadtxt('../results/s12_per.dat')/1e6
so_per = np.loadtxt('../results/so_per.dat')/1e6
c_per = np.loadtxt('../results/u2_per.dat')


plt.figure(1)
plt.plot(cs, sigma_par[:, 0], linestyle='-', color="#ff4b4b" , label="Analytical $ \sigma_{11}$")
plt.plot(c_par, s11_par, marker='s', color="#ff0000", mfc='none', linewidth=0, label="Abaqus $\sigma_{11}$")
plt.plot(cs, sigma_par[:, 1], linestyle='--', color="#694bff", label="Analytical $ \sigma_{22}$")
plt.plot(c_par, s22_par, marker='o', color="#2a00ff", mfc='none', linewidth=0, label="Abaqus $\sigma_{22}$")
plt.plot(cs, -sigma_par[:, 2], linestyle='-.', color="#ffc34b", label="Analytical $ -\sigma_{33}$")
plt.plot(c_par, -s33_par, marker='^', color="#ffaa00", mfc='none', linewidth=0, label="Abaqus $-\sigma_{33}$")
plt.plot(cs, sigma_par[:, 3], linestyle=':', color="#32aa32", label="Analytical $ \sigma_{12}$")
plt.plot(c_par, s12_par, marker='d', color="#00aa00", mfc='none', linewidth=0, label="Abaqus $\sigma_{12}$")

plt.plot(c_par, so_par, marker='h', color="#cece00", mfc='none', linewidth=0, label="Abaqus $||\\boldsymbol{\sigma}_{other}||$")

plt.legend()
plt.ylabel("$\sigma$ ($MPa$)")
plt.xlabel("$c$")
plt.grid()
plt.title("Plain Strain Shear Loading with a Fibre Direction of $\\boldsymbol{a}_0=[\\begin{array}{c c c} 1 & 0 & 0 \end{array}]^T$")
plt.tight_layout()

plt.figure(2)
plt.plot(cs, sigma_per[:, 0], linestyle='-', color="#ff4b4b" , label="Analytical $ \sigma_{11}$")
plt.plot(c_per, s11_per, marker='s', color="#ff0000", mfc='none', linewidth=0, label="Abaqus $\sigma_{11}$")
plt.plot(cs, sigma_per[:, 1], linestyle='--', color="#694bff", label="Analytical $ \sigma_{22}$")
plt.plot(c_per, s22_per, marker='o', color="#2a00ff", mfc='none', linewidth=0, label="Abaqus $\sigma_{22}$")
plt.plot(cs, -sigma_per[:, 2], linestyle='-.', color="#ffc34b", label="Analytical $ -\sigma_{33}$")
plt.plot(c_per, -s33_per, marker='^', color="#ffaa00", mfc='none', linewidth=0, label="Abaqus $-\sigma_{33}$")
plt.plot(cs, sigma_per[:, 3], linestyle=':', color="#32aa32", label="Analytical $ \sigma_{12}$")
plt.plot(c_per, s12_per, marker='d', color="#00aa00", mfc='none', linewidth=0, label="Abaqus $\sigma_{12}$")

plt.plot(c_per, so_per, marker='h', color="#cece00", mfc='none', linewidth=0, label="Abaqus $||\\boldsymbol{\sigma}_{other}||$")

plt.legend()
plt.ylabel("$\sigma$ ($MPa$)")
plt.xlabel("$c$")
plt.grid()
plt.title("Plain Strain Shear Loading with a Fibre Direction of $\\boldsymbol{a}_0=[\\begin{array}{c c c} 0 & 1 & 0 \end{array}]^T$")
plt.tight_layout()


plt.tight_layout()
plt.show()
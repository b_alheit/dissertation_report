import numpy as np


def sigma_bar_aniso_old(F, a0, k1, k2):
    J = np.linalg.det(F)
    a_bar = np.matmul(F, a0) * J ** (-1 / 3)
    C_bar = np.matmul(F.T, F) * J ** (-2 / 3)
    I4_bar = np.matmul(a0.T, np.matmul(C_bar, a0))
    sigma_bar = (2.0 / J) * (k1 * (I4_bar - 1) * np.e ** (k2 * (I4_bar - 1) ** 2)) * (np.outer(a_bar, a_bar) - 1 / 3 * I4_bar * np.eye(3, 3))

    return sigma_bar


def sigma_bar_iso(F, mus, alphas):
    J = np.linalg.det(F)
    b_bar = np.matmul(F, F.T) * J ** (-2/3)
    lambdas_bars, ns = np.linalg.eig(b_bar)
    lambdas_bars = lambdas_bars ** 0.5

    sigma_bar = np.zeros([3, 3])
    lamba_sums = np.outer(lambdas_bars, np.ones(np.alen(mus)))
    lamba_sums = -1/3 * np.sum(lamba_sums**alphas, axis=0)

    for i in range(3):
        coeff = np.sum(mus * (lambdas_bars[i] ** alphas + lamba_sums))
        sigma_bar += coeff * np.outer(ns[:, i], ns[:, i])

    sigma_bar /= J
    return sigma_bar


def sigma_vol(F, K):
    J = np.linalg.det(F)
    sigma = K * (J - 1) * np.eye(3, 3)
    return sigma


def sigma(F, a0, k1, k2, K, mus, alphas):
    iso = sigma_bar_iso(F, mus, alphas)
    aniso = sigma_bar_aniso_old(F, a0, k1, k2)
    vol = sigma_vol(F, K)

    result = iso + aniso + vol
    return result

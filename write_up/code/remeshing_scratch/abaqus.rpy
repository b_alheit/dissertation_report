# -*- coding: mbcs -*-
#
# Abaqus/CAE Release 6.14-1 replay file
# Internal Version: 2014_06_04-21.37.49 134264
# Run by cerecam on Tue Jun 18 13:35:44 2019
#

# from driverUtils import executeOnCaeGraphicsStartup
# executeOnCaeGraphicsStartup()
#: Executing "onCaeGraphicsStartup()" in the site directory ...
from abaqus import *
from abaqusConstants import *
session.Viewport(name='Viewport: 1', origin=(0.0, 0.0), width=295.698303222656, 
    height=134.261108398438)
session.viewports['Viewport: 1'].makeCurrent()
session.viewports['Viewport: 1'].maximize()
from caeModules import *
from driverUtils import executeOnCaeStartup
executeOnCaeStartup()
session.viewports['Viewport: 1'].partDisplay.geometryOptions.setValues(
    referenceRepresentation=ON)
execfile('abaqusMacros.py', __main__.__dict__)
#: The model database "/home/cerecam/Benjamin_Alheit/Projects/masters-disertation/dissertation_report/write_up/code/remeshing_scratch/remeshing.cae" has been opened.
session.viewports['Viewport: 1'].setValues(displayedObject=None)
#: The model database has been saved to "/home/cerecam/Benjamin_Alheit/Projects/masters-disertation/dissertation_report/write_up/code/remeshing_scratch/remeshing.cae".
#: The model database has been saved to "/home/cerecam/Benjamin_Alheit/Projects/masters-disertation/dissertation_report/write_up/code/remeshing_scratch/remeshing.cae".

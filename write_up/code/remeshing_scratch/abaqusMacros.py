# -*- coding: mbcs -*-
# Do not delete the following import lines
from abaqus import *
from abaqusConstants import *
import __main__

def Macro1():
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior
    openMdb(pathName='/home/cerecam/Benjamin_Alheit/Projects/masters-disertation/dissertation_report/write_up/code/remeshing_scratch/remeshing.cae')
    mdb.save()
    a = mdb.models['Model-1'].rootAssembly
    partInstances =(a.instances['collagen-1'], a.instances['keratin-1'],
        a.instances['left-bone-1'], a.instances['right-bone-1'], )
    a.generateMesh(regions=partInstances)
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=124.32,
    #     farPlane=167.177, width=55.3146, height=26.5901, viewOffsetX=5.52699,
    #     viewOffsetY=1.6425)
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=110.57,
    #     farPlane=176.502, width=49.1966, height=23.6491, cameraPosition=(
    #     -74.8815, -97.9808, 87.4987), cameraUpVector=(-0.0644533, 0.906126,
    #     0.418068), cameraTarget=(-4.31356, -1.68436, 5.25361),
    #     viewOffsetX=4.91568, viewOffsetY=1.46083)
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=120.816,
    #     farPlane=168.018, width=53.7552, height=25.8404, cameraPosition=(
    #     -29.0481, -67.0266, 135.592), cameraUpVector=(-0.0141246, 0.993456,
    #     0.113337), cameraTarget=(-6.03103, -3.42793, 7.36377),
    #     viewOffsetX=5.37117, viewOffsetY=1.59619)
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=125.078,
    #     farPlane=163.755, width=17.594, height=8.45755, viewOffsetX=1.80062,
    #     viewOffsetY=5.00695)
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=125.321,
    #     farPlane=164.175, width=17.6281, height=8.47393, cameraPosition=(
    #     -33.0023, -44.2502, 144.833), cameraUpVector=(0.104224, 0.993815,
    #     -0.038318), cameraTarget=(-6.69257, -3.65347, 8.17053),
    #     viewOffsetX=1.80411, viewOffsetY=5.01665)
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=125.407,
    #     farPlane=164.087, width=17.2947, height=8.31364, viewOffsetX=1.76314,
    #     viewOffsetY=5.01273)
    a = mdb.models['Model-1'].rootAssembly
    c1 = a.instances['collagen-1'].cells
    cells1 = c1.getSequenceFromMask(mask=('[#1 ]', ), )
    c2 = a.instances['right-bone-1'].cells
    cells2 = c2.getSequenceFromMask(mask=('[#1 ]', ), )
    pickedRegions = cells1+cells2
    a.generateMesh(regions=pickedRegions, boundaryMeshOverride=ON)
    mdb.save()

def Macro2():
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior
    pass

def Macro4():
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior
    openMdb(pathName='/home/cerecam/Benjamin_Alheit/Projects/masters-disertation/dissertation_report/write_up/code/remeshing_scratch/remeshing.cae')
    session.viewports['Viewport: 1'].setValues(displayedObject=None)
    p = mdb.models['Model-1'].parts['collagen']
    session.viewports['Viewport: 1'].setValues(displayedObject=p)
    session.viewports['Viewport: 1'].partDisplay.setValues(mesh=ON)
    session.viewports['Viewport: 1'].partDisplay.meshOptions.setValues(
        meshTechnique=ON)
    session.viewports['Viewport: 1'].partDisplay.geometryOptions.setValues(
        referenceRepresentation=OFF)
    a = mdb.models['Model-1'].rootAssembly
    session.viewports['Viewport: 1'].setValues(displayedObject=a)
    session.viewports['Viewport: 1'].assemblyDisplay.setValues(mesh=ON,
        optimizationTasks=OFF, geometricRestrictions=OFF, stopConditions=OFF)
    session.viewports['Viewport: 1'].assemblyDisplay.meshOptions.setValues(
        meshTechnique=ON)

Macro1()
